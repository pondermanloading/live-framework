package org.live4j.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Title:加载配置文件
 * Description:提供指定的配置文件对象，并提供根据配置项获取不同类型值的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/19 20:24
 */
public class PropsUtil {

    private static final Logger logger = LoggerFactory.getLogger(PropsUtil.class);

    /**
     * 加载配置文件对象
     * @return
     */
    public static Properties loadProps(String propsPath){
        Properties props = new Properties();
        InputStream in = null;
        try{
            // 1.文件路径是否为空
            if(StringUtil.isEmpty(propsPath)){
                throw new IllegalArgumentException("配置文件名为空");
            }
            // 2.文件后缀是否正确
            String suffix = ".properties";
            if(!propsPath.endsWith(suffix)){
                throw new IllegalArgumentException("文件后缀不正确，请以.properties结尾");
            }
            in = ClassUtil.getClassLoader().getResourceAsStream(propsPath);
            if(null == in){
                throw new FileNotFoundException("文件位置不正确");
            }
            props.load(in);
        }catch(Exception ex){
            logger.error("获取配置文件对象失败" + ex);
            throw new RuntimeException("获取配置文件对象失败" + ex);
        }finally{
            try{
                if(null != in){
                    in.close();
                }
            }catch(IOException ex){
                logger.error("读取配置文件的流关闭失败" + ex);
            }
        }
        return props;
    }

    /**
     * 根据配置项，获得对应的值
     * @param props
     * @param key
     * @return
     */
    public static String getString(Properties props, String key){
        String value = "";
        if(props.containsKey(key)){
            value = props.getProperty(key);
        }
        return value;
    }

    /**
     * 根据配置项，获得对应的值(可以有默认值)
     * @param props
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getString(Properties props, String key, String defaultValue){
        String value = defaultValue;
        if(props.containsKey(key)){
            value = props.getProperty(key);
        }
        return value;
    }

    /**
     * 根据配置项，获得对应的值
     * @param props
     * @param key
     * @return
     */
    public static int getInt(Properties props, String key){
        int value = 0;
        if(props.containsKey(key)){
            value = CastUtil.castInt(props.getProperty(key));
        }
        return value;
    }

    /**
     * 根据配置项，获得对应的值(可以有默认值)
     * @param props
     * @param key
     * @param defaultValue
     * @return
     */
    public static int getInt(Properties props, String key, int defaultValue){
        int value = defaultValue;
        if(props.containsKey(key)){
            value = CastUtil.castInt(props.getProperty(key));
        }
        return value;
    }

    /**
     * 根据配置项，获得对应的值
     * @param props
     * @param key
     * @return
     */
    public static boolean getBoolean(Properties props, String key){
        boolean value = false;
        if(props.containsKey(key)){
            value = CastUtil.castBoolean(props.getProperty(key));
        }
        return value;
    }

    /**
     * 根据配置项，获得对应的值(可以有默认值)
     * @param props
     * @param key
     * @param defaultValue
     * @return
     */
    public static boolean getBoolean(Properties props, String key, boolean defaultValue){
        boolean value = defaultValue;
        if(props.containsKey(key)){
            value = CastUtil.castBoolean(props.getProperty(key));
        }
        return value;
    }

    /**
     * 获取指定前缀配置项的配置项和值
     * @param props
     * @param prefix
     * @return
     */
    public static Map<String, Object> getMap(Properties props, String prefix){
        Map<String, Object> kvMap = new HashMap<String, Object>();
        Set<String> keySet = props.stringPropertyNames();
        if(CollectionUtil.isNotEmpty(keySet)){
            for(String key : keySet){
                if(key.startsWith(prefix)){
                    String value = props.getProperty(key);
                    kvMap.put(key, value);
                }
            }
        }
        return kvMap;
    }

}
