package org.live4j.framework.util;

import org.apache.commons.collections.MapUtils;
import org.omg.CosNaming.NamingContextPackage.NotEmpty;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Title: 映射集合工具类
 * Description: 封装映射相关的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 16:29
 */
public class MapUtil {

    /**
     * 判断Map是否为空
     * @param map 指定map
     * @return 布尔值
     */
    public static boolean isEmpty(Map<?,?> map){
        return MapUtils.isEmpty(map);
    }

    /**
     * 判断Map是否不为空
     * @param map 指定map
     * @return 布尔值
     */
    public static boolean isNotEmpty(Map<?,?> map){
        return !MapUtils.isEmpty(map);
    }

    /**
     * @param source 原Map
     * @return 转置Map
     */
    public static <K, V> Map<V, K> invert(Map<K, V> source){
        Map<V, K> target = null;
        if(isNotEmpty(source)){
            target = new LinkedHashMap<V, K>(source.size());
            for(Map.Entry<K, V> entry : source.entrySet()){
                target.put(entry.getValue(), entry.getKey());
            }
        }
        return target;
    }
}
