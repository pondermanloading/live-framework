package org.live4j.framework.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Title: 文件操作工具类
 * Description: 封装文件操作接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/25 20:37
 */
public class FileUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 返回文件名(自动去除文件路径)
     * @param fileName 文件名
     * @return
     */
    public static String getRealFileName(String fileName){
        return FilenameUtils.getName(fileName);
    }

    /**
     * 创建文件对象
     * @param filePath 文件路径
     * @return 文件对象
     */
    public static File createFile(String filePath){
        File file;
        try{
            // 创建指定文件路径对象
            file = new File(filePath);
            // 是否创建文件所在目录
            File parentDir = file.getParentFile();
            if(!parentDir.exists()){
                FileUtils.forceMkdir(parentDir);
            }
        }catch(Exception ex){
            logger.error("创建文件对象失败" + ex);
            throw new RuntimeException("创建文件对象失败" + ex);
        }
        return file;
    }
}
