package org.live4j.framework.util;

import org.apache.commons.lang.StringUtils;
import org.omg.CORBA.portable.InputStream;
import org.omg.CORBA.portable.OutputStream;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Title:字符串工具类
 * Description:提供操作字符串相关的工具类
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/19 20:30
 */
public class StringUtil {

    public static final String SEPARATOR = String.valueOf((char) 29);

    /**
     * 字符串是否不为空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str){
        return StringUtils.isNotEmpty(str);
    }

    /**
     * 字符串是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        return StringUtils.isEmpty(str);
    }

    /**
     * 根据固定分隔符分割字符串
     * @param str 待分割的字符串
     * @param separator 分隔符
     * @return 分割后的字符串数组
     */
    public static String[] splitString(String str, String separator){
        return StringUtils.splitByWholeSeparator(str, separator);
    }

    /**
     * 如果目标字符串为NULL/0，返回默认字符串
     * @param targetStr
     * @param defaultStr
     * @return
     */
    public static String defaultIfEmpty(String targetStr, String defaultStr){
        return StringUtils.defaultIfEmpty(targetStr, defaultStr);
    }

    /**
     * 替换固定格式的字符串（支持正则表达式）
     */
    public static String replaceAll(String target, String regex, String replacement) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(target);
        StringBuffer buffer = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(buffer, replacement);
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }

    /**
     * 将驼峰风格替换为下划线风格
     * （1）find()匹配到的元组[字符或字符串]
     * （2）start()匹配到的当前元组[字符或字符串]的第一个字符的索引
     * （3）end()匹配到的当前元组[字符或字符串]的最后一个字符的后一个索引
     * （4）group()匹配到的元组[字符或字符串]
     * （5）charAt(0)第1个字符
     */
    public static String camelhumpToUnderline(String str) {
        Matcher matcher = Pattern.compile("[A-Z]").matcher(str);
        StringBuilder builder = new StringBuilder(str);
        for (int i = 0; matcher.find(); i++) {
            // .replace(start, end, str)，在字符的start位置（包括start），到end的位置（不包括end），将这之间的字符串，替换成str
            // +i是因为每找到一个元组，进行替换后，字符串都会多一个'_'，所以字符串长度会+i，相应的索引位置需要+i
            //System.out.println("---"+matcher.start()+"---"+matcher.end()+"---"+matcher.group());
            builder.replace(matcher.start() + i, matcher.end() + i,
                    "_" + matcher.group().toLowerCase());
        }
        if (builder.charAt(0) == '_') {
            builder.deleteCharAt(0);
        }
        return builder.toString();
    }

}
