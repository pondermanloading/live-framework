package org.live4j.framework.util;

import org.live4j.framework.core.ConfigConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Title: 处理编码类
 * Description: 封装编码相关的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 10:10
 */
public class CodeUtil {
    private static final Logger logger = LoggerFactory.getLogger(CodeUtil.class);

    /**
     * 返回解码后的字符串
     * @param url 编码的字符串
     * @return 解码后的字符串
     */
    public static String decodeURL(String url){
        String target;
        try {
            target = URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            logger.error("解码异常" + ex);
            throw new RuntimeException("解码异常" + ex);
        }
        return target;
    }

    /**
     * 编码
     * @param url
     * @return
     */
    public static String encodeURL(String url){
        String target = "";
        try {
            target = URLEncoder.encode(url, ConfigConstant.DEFAULT_CODE);
        } catch (UnsupportedEncodingException ex) {
            logger.error("编码异常" + ex);
        }
        return target;
    }


}
