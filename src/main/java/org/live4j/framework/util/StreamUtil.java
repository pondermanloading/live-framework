package org.live4j.framework.util;

import org.live4j.framework.core.ConfigConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Title: 处理流
 * Description: 封装处理流的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 09:56
 */
public class StreamUtil {
    private static final Logger logger = LoggerFactory.getLogger(StreamUtil.class);

    /**
     * 将字节流转化成字符串
     * @param in
     * @return
     */
    public static String getString(InputStream in){
        StringBuilder builder = new StringBuilder();
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = reader.readLine()) != null){
                builder.append(line);
            }
        }catch(IOException ex){
            logger.error("数据流转化成字符串失败" + ex);
            throw new RuntimeException("数据流转化成字符串失败" + ex);
        }
        return builder.toString();
    }


    /**
     * 将输入流复制到输出流
     * @param inputStream
     * @param outputStream
     */
    public static void copyStream(InputStream inputStream, OutputStream outputStream){
        try {
            int length;
            // 缓冲区
            byte[] buffer = new byte[ConfigConstant.DEFAULT_BYTE_SIZE];
            while((length = inputStream.read(buffer, 0, buffer.length)) != -1){
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
        } catch (IOException ex) {
            logger.error("输入流复制到输出流异常" + ex);
            throw new RuntimeException("输入流复制到输出流异常" + ex);
        }finally{
            try {
                if(null != inputStream)
                    inputStream.close();
                if(null != outputStream)
                    outputStream.close();
            } catch (IOException ex) {
                logger.error("将输入流复制到输出流时关闭流异常" + ex);
                throw new RuntimeException("将输入流复制到输出流时关闭流异常" + ex);
            }
        }
    }
}
