package org.live4j.framework.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: 序列化/反序列化工具类
 * Description: 提供序列化和反序列化的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 10:37
 */
public class JsonUtil {
    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    /**
     * 序列化
     * @param obj 指定序列化的对象
     * @return 序列化json字符串
     */
    public static String toJson(Object obj){
        String target;
        try {
            target = OBJECT_MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            logger.error("序列化失败" + ex);
            throw new RuntimeException("序列化失败" + ex);
        }
        return target;
    }


}
