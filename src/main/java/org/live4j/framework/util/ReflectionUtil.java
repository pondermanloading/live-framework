package org.live4j.framework.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * Title: 反射操作工具类
 * Description: 提供反射相关操作的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/21 19:55
 */
public class ReflectionUtil {
    private static final Logger logger = LoggerFactory.getLogger(ReflectionUtil.class);

    /**
     * 创建指定加载类的对象
     * @param cls 指定Class类
     * @return 指定Class类的对象
     */
    public static Object newInstanceByClass(Class<?> cls){
        Object instance;
        try{
            instance = cls.newInstance();
        }catch(Exception ex){
            logger.error("初始化Class对象失败" + ex);
            throw new RuntimeException("初始化Class对象失败" + ex);
        }
        return instance;
    }

    /**
     * 创建指定加载类的对象
     * @param className Class的完全限定名
     * @param <T> Class类型
     * @return
     */
    public static <T> T newInstanceByClassName(String className){
        T instance;
        try {
            Class<?> cls = ClassUtil.loadClass(className);
            instance = (T) cls.newInstance();
        } catch (Exception ex) {
            logger.error("创建指定Class类的对象异常" + ex);
            throw new RuntimeException("创建指定Class类的对象异常" + ex);
        }
        return instance;
    }

    /**
     * 调用对象的方法
     * @param obj 指定对象
     * @param method 方法对象
     * @param args 方法参数
     */
    public static Object invokeMethod(Object obj, Method method, Object... args){
        Object result;
        try{
            method.setAccessible(true);// 取消安全监测，提高执行效率
            result = method.invoke(obj, args);// 调用方法的返回值
        }catch(Exception ex){
            logger.error("调用反射方法失败" + ex);
            throw new RuntimeException("调用反射方法失败" + ex);
        }
        return result;
    }

    /**
     * 设置成员变量
     * @param obj 指定对象
     * @param field 成员变量
     * @param value 值
     */
    public static void setField(Object obj, Field field, Object value){
        try{
            field.setAccessible(true);
            field.set(obj, value);
        }catch(Exception ex){
            logger.error("设置成员变量失败" + ex);
            throw new RuntimeException("设置成员变量失败" + ex);
        }
    }

}
