package org.live4j.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: 加载类的工具类
 * Description:提供加载类相关的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/19 21:05
 */
public class ClassUtil {

    private static final Logger logger = LoggerFactory.getLogger(ClassUtil.class);

    /**
     * 获得当前线程的类加载器
     * @return 类加载器
     */
    public static ClassLoader getClassLoader(){
        return Thread.currentThread().getContextClassLoader();
    }


    /**
     * 加载类
     * @param className 指定类的完全限定名
     * @return
     */
    public static Class<?> loadClass(String className){
        return loadClass(className, true);
    }

    /**
     * 加载类
     * @param className 指定类的完全限定名
     * @param isInitialized 是否初始化
     */
    public static Class<?> loadClass(String className, boolean isInitialized){
        Class<?> cls;
        try {
            cls = Class.forName(className, isInitialized, getClassLoader());
        } catch (ClassNotFoundException ex) {
            logger.error("加载类时类型转换异常" + ex);
            throw new RuntimeException("加载类时类型转换异常" + ex);
        }
        return cls;
    }

}
