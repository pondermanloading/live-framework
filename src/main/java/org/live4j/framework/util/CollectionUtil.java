package org.live4j.framework.util;

import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;

/**
 * Title: 集合操作类
 * Description: 判断集合相关的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/20 08:20
 */
public class CollectionUtil {

    /**
     * 判断集合是否不为空
     * @param coll
     * @return
     */
    public static boolean isNotEmpty(Collection<?> coll){
        return CollectionUtils.isNotEmpty(coll);
    }

    /**
     * 判断集合是否为空
     * @param coll
     * @return
     */
    public static boolean isEmpty(Collection<?> coll){
        return CollectionUtil.isEmpty(coll);
    }

}
