package org.live4j.framework.util;


import org.live4j.framework.core.ConfigConstant;
import org.live4j.framework.core.ConfigHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Title: Web功能工具类
 * Description: 操作Web相关接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/29 08:26
 */
public class WebUtil {
    private static final Logger logger = LoggerFactory.getLogger(WebUtil.class);


    /**
     * 返回Json字符串（Json格式）
     * @param response 响应
     * @param obj 处理对象
     */
    public static void writeJson(HttpServletResponse response, Object obj){
        PrintWriter writer = null;
        try{
            writer = response.getWriter();
            response.setContentType("application/json");// 内容格式
            response.setCharacterEncoding(ConfigConstant.DEFAULT_CODE);// 编码
            String json = JsonUtil.toJson(obj);// json字符串
            writer.write(json);
            writer.flush();
        }catch(IOException ex){
            logger.error("返回Json数据对象异常" + ex);
            throw new RuntimeException("返回Json数据对象异常" + ex);
        }finally {
            if(null != writer){
                writer.close();
            }
        }
    }

    /**
     * 返回Json字符串（HTML格式）
     * @param response
     * @param obj
     */
    public static void writeHTML(HttpServletResponse response, Object obj){
        PrintWriter writer = null;
        try{
            writer = response.getWriter();
            response.setContentType("text/html");// 内容格式
            response.setCharacterEncoding(ConfigConstant.DEFAULT_CODE);// 编码
            String json = JsonUtil.toJson(obj);
            writer.write(json);
            writer.flush();
        }catch(IOException ex){
            logger.error("返回Json字符串异常" + ex);
            throw new RuntimeException("返回Json字符串异常" + ex);
        }finally{
            if(null != writer){
                writer.close();
            }
        }
    }

    /**
     * 请求重定向
     * @param request 请求
     * @param response 响应
     * @param path 请求路径
     */
    public static void redirectRequest(HttpServletRequest request ,HttpServletResponse response, String path){
        try {
            response.sendRedirect(request.getContextPath() + path);
        } catch (IOException ex) {
            logger.error("请求重定向异常" + ex);
            throw new RuntimeException("请求重定向异常" + ex);
        }
    }

    /**
     * 请求转发
     * @param request 请求
     * @param response 响应
     * @param path 请求路径
     */
    public static void forwardRequest(HttpServletRequest request, HttpServletResponse response, String path){
        try {
            request.getRequestDispatcher(ConfigHelper.getAppJspPath() + path).forward(request, response);
        } catch (ServletException ex) {
            logger.error("请求转发异常" + ex);
            throw new RuntimeException("请求转发异常" + ex);
        } catch (IOException ex) {
            logger.error("请求转发异常" + ex);
            throw new RuntimeException("请求转发异常" + ex);
        }
    }

    /**
     * 是否是Ajax请求
     * @param request 请求
     * @return 布尔值
     */
    public static boolean isAjax(HttpServletRequest request){
        return request.getHeader("X-Requested-With") != null;
    }

    /**
     * 发送错误代码
     * @param errorCode 错误代码
     * @param msg 错误信息
     * @param response 响应
     */
    public static void sendError(int errorCode, String msg, HttpServletResponse response){
        try {
            response.sendError(errorCode, msg);
        } catch (IOException ex) {
            logger.error("发送错误代码异常" + ex);
            throw new RuntimeException("发送错误代码异常" + ex);
        }
    }

    /**
     * 返回请求路径
     * @param request
     * @return
     */
    public static String getRequestPath(HttpServletRequest request){
        String servletPath = request.getServletPath();
        String pathInfo = StringUtil.defaultIfEmpty(request.getPathInfo(), "");
        return servletPath + pathInfo;
    }

}
