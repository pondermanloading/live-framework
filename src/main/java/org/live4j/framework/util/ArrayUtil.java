package org.live4j.framework.util;

import org.apache.commons.lang.ArrayUtils;

/**
 * Title: 操作数组
 * Description:提供操作数据的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 08:44
 */
public class ArrayUtil {

    /**
     * 判断数组是否不为空
     * @param array
     * @return
     */
    public static boolean isNotEmpty(Object[] array){
        return !ArrayUtils.isEmpty(array);
    }

    /**
     * 判断数组是否为空
     * @param array
     * @return
     */
    public static boolean isEmpty(Object[] array){
        return ArrayUtil.isEmpty(array);
    }
}
