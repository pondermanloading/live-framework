package org.live4j.framework.util;

import com.sun.org.apache.xml.internal.utils.StringToStringTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: 类型转换工具类
 * Description: 提供各种类型的转换接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/19 21:16
 */
public class CastUtil {

    private static final Logger logger = LoggerFactory.getLogger(CastUtil.class);

    /**
     * 转换成 String类型
     * @param obj
     * @return
     */
    public static String castString(Object obj){
        return castString(obj, "");
    }

    /**
     * 转换成 String类型(可以有默认值)
     * @param obj
     * @return
     */
    public static String castString(Object obj, String defaultValue){
        return obj == null ? defaultValue : String.valueOf(obj);
    }

    /**
     * 转换成 int类型
     * @param obj
     * @return
     */
    public static int castInt(Object obj){
        return castInt(obj, 0);
    }

    /**
     * 转换成 int类型(可以有默认值)
     * @param obj
     * @return
     */
    public static int castInt(Object obj, int defaultValue){
        int intValue = defaultValue;
        if(null != obj){
            String strValue = castString(obj);
            if(StringUtil.isNotEmpty(strValue)){
                try{
                    intValue = Integer.parseInt(strValue);
                }catch(NumberFormatException ex){
                    logger.error("转换int类型失败" + ex);
                }
            }
        }
        return intValue;
    }

    /**
     * 转换成 boolean类型
     * @param obj
     * @return
     */
    public static boolean castBoolean(Object obj){
        return castBoolean(obj, false);
    }

    /**
     * 转换成 boolean类型(可以有默认值)
     * @param obj
     * @param defaultValue
     * @return
     */
    public static boolean castBoolean(Object obj, boolean defaultValue){
        boolean boolValue = defaultValue;
        if(null != obj){
            String strValue = castString(obj);
            if(StringUtil.isNotEmpty(strValue)){
                boolValue = Boolean.valueOf(strValue);
            }
        }
        return boolValue;
    }

    /**
     * 转换成 double类型
     * @param obj
     * @return
     */
    public static double castDouble(Object obj){
        return castDouble(obj, 0.0);
    }

    /**
     * 转换成 double类型(可以有默认值)
     * @param obj
     * @param defaultValue
     * @return
     */
    public static double castDouble(Object obj, double defaultValue){
        double doubleValue = defaultValue;
        if(null != obj){
            String strValue = castString(obj);
            if(StringUtil.isNotEmpty(strValue)){
                doubleValue = Double.valueOf(strValue);
            }
        }
        return doubleValue;
    }

}
