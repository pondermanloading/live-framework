package org.live4j.framework.filter.file;

import java.io.File;
import java.io.FileFilter;

/**
 * Title: 文件过滤器
 * Description: 文件后缀过滤器
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/27 15:44
 */
public class FileFilterBySuffix implements FileFilter{
    // 文件后缀
    private String suffix;

    public FileFilterBySuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * 根据文件后缀过滤
     * @param file 指定文件
     * @return
     */
    public boolean accept(File file){
        return file.getName().endsWith(suffix);
    }
}
