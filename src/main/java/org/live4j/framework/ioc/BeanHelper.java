package org.live4j.framework.ioc;

import org.live4j.framework.core.scanner.ClassHelper;
import org.live4j.framework.util.ReflectionUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Title: Bean容器
 * Description: 提供所有标记注解的加载类的单例实例对象
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/21 19:51
 */
public final class BeanHelper {
    // 单例容器
    private static final Map<Class<?>, Object> BEAN_MAP = new HashMap<Class<?>, Object>();

    static{
        // 返回所有标记注解的加载类
        Set<Class<?>> classSet = ClassHelper.getBeanClassSet();
        for(Class<?> cls : classSet){
            Object obj = ReflectionUtil.newInstanceByClass(cls);
            if(null != obj){
                setBean(cls, obj);
            }
        }
    }

    /**
     * 设置加载类与对应实例的映射关系
     * @param cls
     * @param obj
     */
    public static void setBean(Class<?> cls, Object obj){
        BEAN_MAP.put(cls, obj);
    }

    /**
     * 返回Bean容器
     * @return Bean容器
     */
    public static Map<Class<?>, Object> getBeanMap(){
        return BEAN_MAP;
    }

    /**
     * 返回指定加载类的实例对象
     * @param cls 指定加载类
     * @param <T> 指定类型
     * @return 指定Class类的实例对象
     */
    public static <T> T getBean(Class<T> cls){
        if(!BEAN_MAP.containsKey(cls)){
            throw new RuntimeException("Bean容器中不存在 " + cls);
        }
        return (T) BEAN_MAP.get(cls);
    }

}
