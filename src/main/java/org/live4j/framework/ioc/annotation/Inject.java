package org.live4j.framework.ioc.annotation;

import javax.annotation.Resource;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Title: 注入注解
 * Description: 提供注入功能的标识
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/21 19:01
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
}
