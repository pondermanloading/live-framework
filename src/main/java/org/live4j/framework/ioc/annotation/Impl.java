package org.live4j.framework.ioc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Title: 指定注入的实现类
 * Description: 在接口上标识需要注入的实现类
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 16:26
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Impl {
    Class<?> value();
}
