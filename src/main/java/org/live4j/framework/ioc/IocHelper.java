package org.live4j.framework.ioc;


import org.live4j.framework.core.scanner.ClassHelper;
import org.live4j.framework.ioc.annotation.Impl;
import org.live4j.framework.ioc.annotation.Inject;
import org.live4j.framework.util.ArrayUtil;
import org.live4j.framework.util.CollectionUtil;
import org.live4j.framework.util.RandomUtil;
import org.live4j.framework.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

/**
 * Title: Ioc框架
 * Description: 实现注入
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 08:13
 */
public final class IocHelper {
    static {
        Map<Class<?>, Object> beanMap = BeanHelper.getBeanMap();
        for(Map.Entry<Class<?>, Object> beanEntry : beanMap.entrySet()){
            Class<?> beanClass = beanEntry.getKey();// 加载类
            Object beanInstance = beanEntry.getValue();// 加载类的实例对象
            Field[] fields = beanClass.getDeclaredFields();// 加载类的所有属性对象
            if(ArrayUtil.isNotEmpty(fields)){// 判断当前属性数组是否不为空
                for(Field beanField : fields){
                    if(beanField.isAnnotationPresent(Inject.class)){// 判断当前属性对象是否有@Inject注解
                        Class<?> beanFieldClass = beanField.getType();// 返回当前属性的加载类（接口或者实现类）
                        Class<?> implementClass = findImplementClass(beanFieldClass);// 返回指定接口的某一个实现类，或返回实现类
                        if(null != implementClass){
                            Object beanFieldInstance = beanMap.get(implementClass);// 返回加载类（接口的一个实现类或具体实现类）对应的实例对象
                            if(null != beanFieldInstance){
                                ReflectionUtil.setField(beanInstance, beanField, beanFieldInstance);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 1.如果加载类是接口，则根据@Impl注解返回指定的加载类，或者没有@Impl注解，则返回接口的任意一个实现类的加载类
     * 2.如果加载类是实现类，则直接返回加载类
     * @param beanFieldCLass 加载类
     * @return 加载类
     */
    private static Class<?> findImplementClass(Class<?> beanFieldCLass){
        Class<?> implementClass = beanFieldCLass;// 默认返回实现类
        if(beanFieldCLass.isInterface()) {
            if (beanFieldCLass.isAnnotationPresent(Impl.class)) {// 如果有@Impl注解
                // 返回接口上强制指定的实现类的加载类
                implementClass = beanFieldCLass.getAnnotation(Impl.class).value();
            } else {// 如果没有@Impl注解
                // 返回指定接口的Class类的所有实现类
                Set<Class<?>> implementClassSet = ClassHelper.getClassSetBySuper(beanFieldCLass);
                if (CollectionUtil.isNotEmpty(implementClassSet)) {
                    implementClass = RandomUtil.getRandomElement(implementClassSet);// 随机返回一个
                }
            }
        }
        return implementClass;
    }

}
