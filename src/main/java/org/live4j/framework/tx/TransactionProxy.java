package org.live4j.framework.tx;

import org.live4j.framework.aop.proxy.Proxy;
import org.live4j.framework.aop.proxy.ProxyChain;
import org.live4j.framework.tx.annotation.Transaction;
import org.live4j.framework.dao.DataBaseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * Title: 事务代理
 * Description: 封装执行事务横切逻辑接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/19 16:09
 */
public class TransactionProxy implements Proxy {
    private static final Logger logger = LoggerFactory.getLogger(TransactionProxy.class);

    // 判断是否开启事务
    private static ThreadLocal<Boolean> FLAG = new ThreadLocal<Boolean>(){
        @Override
        public Boolean initialValue(){
            return false;
        }
    };

    /**
     * 执行事务横切逻辑
     * @param proxyChain 代理链
     * @return
     */
    public Object doProxy(ProxyChain proxyChain) throws Throwable {
        Object result = null;
        // 默认false 表示没有开启事务
        boolean flag = FLAG.get();
        // 目标方法对象
        Method method = proxyChain.getTargetMethod();
        // 没有开启事务，并且方法对象有@Transaction注解
        if(!flag && method.isAnnotationPresent(Transaction.class)){
            // 表示已开启事务
            FLAG.set(true);
            try {
                DataBaseHelper.beginTransaction();
                logger.debug("start Transaction");
                result = proxyChain.doProxy();
                logger.debug("close Transaction");
                DataBaseHelper.commitTransaction();
            }catch(Exception ex){
                logger.error("事务异常" + ex);
                DataBaseHelper.rollbackTransaction();
            }
        }else{
            result = proxyChain.doProxy();
        }
        return result;
    }
}
