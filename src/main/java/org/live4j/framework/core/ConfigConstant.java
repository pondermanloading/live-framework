package org.live4j.framework.core;

/**
 * Title:系统配置文件配置项常量
 * Description:提供系统配置文件配置项常量
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/19 19:45
 */
public interface ConfigConstant {
    // 系统配置文件名
    String CONFIG_FILE = "live.properties";
    // 默认编码
    String DEFAULT_CODE = "UTF-8";
    // 默认缓冲数组大小
    int DEFAULT_BYTE_SIZE = 4096;
    // 主页
    String HOME_PAGE = "/index.html";
    // 数据库驱动
    String JDBC_DRIVER = "live.framework.jdbc.driver";
    // 数据库URL
    String JDBC_URL = "live.framework.jdbc.url";
    // 数据库用户名
    String JDBC_USERNAME = "live.framework.jdbc.username";
    // 数据库密码
    String JDBC_PASSWORD = "live.framework.jdbc.password";
    // 数据库活跃状态最大连接数
    String JDBC_MAX_ACTIVE = "live.framework.jdbc.max_active";
    // 空闲状态最大连接数
    String JDBC_MAX_IDLE = "live.framework.jdbc.max_idle";
    // 应用基础包名
    String APP_BASE_PACKAGE = "live.framework.app.base_package";
    // 应用JSP基础路径
    String APP_JSP_PATH = "live.framework.app.jsp_path";
    // 应用静态资源基础路径，包括JS、CSS、图片等
    String APP_ASSET_PATH = "live.framework.app.asset_path";
    // 上传文件大小限制
    String APP_UPLOAD_LIMIT = "live.framwork.app.upload_limit";
}
