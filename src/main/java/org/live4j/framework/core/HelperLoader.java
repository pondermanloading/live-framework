package org.live4j.framework.core;

import org.live4j.framework.aop.AopHelper;
import org.live4j.framework.dao.DataBaseHelper;
import org.live4j.framework.ioc.BeanHelper;
import org.live4j.framework.core.scanner.ClassHelper;
import org.live4j.framework.mvc.ControllerHelper;
import org.live4j.framework.ioc.IocHelper;
import org.live4j.framework.util.ClassUtil;

/**
 * Title: 助手类加载器
 * Description: 统一加载 Helper助手类
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 20:17
 */
public class HelperLoader {

    /**
     * 初始化Helper类
     */
    public static void init(){
        Class<?>[] classList = {
            DataBaseHelper.class,
            ClassHelper.class,
            BeanHelper.class,
            AopHelper.class,
            IocHelper.class,
            ControllerHelper.class
        };
        for(Class<?> cls : classList){
            ClassUtil.loadClass(cls.getName(),false);
        }
    }
}
