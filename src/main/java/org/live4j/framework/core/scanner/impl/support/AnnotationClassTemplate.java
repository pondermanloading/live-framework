package org.live4j.framework.core.scanner.impl.support;

import java.lang.annotation.Annotation;

/**
 * Title: 类加载器的注解模板类
 * Description:
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/25 17:00
 */
public abstract class AnnotationClassTemplate extends ClassTemplate {
    // 注解类
    private Class<? extends Annotation> annotationClass;

    public AnnotationClassTemplate(String packageName, Class<? extends Annotation> annotationClass){
        super(packageName);
        this.annotationClass = annotationClass;
    }
}
