package org.live4j.framework.core.scanner.impl.support;

import org.live4j.framework.filter.file.FileFilterBySuffix;
import org.live4j.framework.util.ClassUtil;
import org.live4j.framework.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Title: 类加载器的模板类
 * Description:
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/25 16:50
 */
public abstract class ClassTemplate {
    private static final Logger logger = LoggerFactory.getLogger(ClassTemplate.class);

    private String packageName;// 包名

    public ClassTemplate(String packageName){
        this.packageName = packageName;
    }

    /**
     * @return 返回指定包下，所有加载类
     */
    public Set<Class<?>> getClassSet(){
        Set<Class<?>> classSet = new HashSet<Class<?>>();// 指定包下的所有类的集合
        try {
            Enumeration<URL> urls = ClassUtil.getClassLoader().getResources(packageName.replace(".","/"));// 将包名转换成物理路径，并获取该路径下的所有URL资源
            while(urls.hasMoreElements()){
                URL url = urls.nextElement();
                if(null != url){
                    String protocol = url.getProtocol();// 返回URL的协议file或jar
                    if("file".equals(protocol)){
                        String packagePath = url.getPath().replaceAll("%20", " ");// 返回指定包的物理路径 file:c:\\MyWork\com\test
                        File packagePathFile = new File(packagePath);// 包文件路径文件对象
                        FileFilter filter = new FileFilterBySuffix(".class");// 文件后缀过滤器
                        addClass(packagePathFile, packageName, classSet, filter);
                    }else if("jar".equals(protocol)){
                        JarURLConnection jarURLConnection = (JarURLConnection) url.openConnection();
                        if(null != jarURLConnection){
                            JarFile jarFile = jarURLConnection.getJarFile();
                            if(null != jarFile){
                                Enumeration<JarEntry> entries = jarFile.entries();
                                while(entries.hasMoreElements()){
                                    JarEntry jarEntry = entries.nextElement();
                                    String entryName = jarEntry.getName();// 名称，如com/test/A.class
                                    if(entryName.endsWith(".class")){// 加载.class结尾的文件
                                        String className = entryName.substring(0, entryName.lastIndexOf(".")).replace("/", ".");
                                        doAddClass(className, classSet);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("加载指定包下的所有Class类异常");
        }
        return classSet;
    }

    /**
     * 加载Class类
     * @param packagePathFile 指定包的物理路径文件对象
     * @param packageName 指定包的文件名
     * @param classSet 存储器
     * @param fileFilter 过滤器
     */
    private void addClass(File packagePathFile, String packageName, Set<Class<?>> classSet, FileFilter fileFilter){
        // 在这不要通过匿名内部类的方式实现一个过滤器，否则在递归时会在内存创建很多过滤器对象，导致堆溢出
        File[] files = packagePathFile.listFiles();// 返回当前物理路径下的所有文件对象。
        for(File file : files){// 遍历所有的文件对象
            String fileName = file.getName();// 文件名，如A.class或文件路径B
            if(file.isFile()){// 如果是文件
                if(fileFilter.accept(file)) {// 当前文件满足过滤条件（以.class结尾的文件）
                    String className = fileName.substring(0, fileName.lastIndexOf("."));
                    if (StringUtil.isNotEmpty(packageName)) {
                        className = packageName + className;// 类的完全限定名，如com.test.A
                    }
                    doAddClass(className, classSet);
                }
            }else if(file.isDirectory()){// 如果是文件路径，如com/test/B
                String subPackageName = fileName;// 子包名
                if(StringUtil.isNotEmpty(packageName)){
                    subPackageName = packageName + "." + fileName;
                }
                addClass(file, subPackageName, classSet, fileFilter);// 递归处理
            }
        }

    }

    /**
     * 过滤目标类，并放到存储容器
     * @param className 类的完全限定名
     * @param classSet 存储容器
     */
    private void doAddClass(String className, Set<Class<?>> classSet){
        Class<?> cls = ClassUtil.loadClass(className);
        if(checkAddClass(cls)){// 过滤目标类
           classSet.add(cls);
        }
    }

    /**
     * 过滤目标加载类
     * @param cls 需要判断的目标加载类
     * @return 是否满足添加的条件
     */
    public abstract boolean checkAddClass(Class<?> cls);
}
