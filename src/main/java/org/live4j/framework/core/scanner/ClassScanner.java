package org.live4j.framework.core.scanner;

import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * Title: 类加载器
 * Description: 返回相关集合的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/25 16:17
 */
public interface ClassScanner {

    /**
     * @param packageName 包名
     * @return 指定包下的所有类
     */
    Set<Class<?>> getClassSet(String packageName);

    /**
     * @param packageName 包名
     * @param superClass 父类/接口
     * @return 指定父类/接口的所有相关类
     */
    Set<Class<?>> getClassSetBySuper(String packageName, Class<?> superClass);

    /**
     * @param packageName 包名
     * @param annotationClass 注解类
     * @return 指定注解类的所有类
     */
    Set<Class<?>> getClassSetByAnnotation(String packageName, Class<? extends Annotation> annotationClass);
}
