package org.live4j.framework.core.scanner.impl;

import org.live4j.framework.core.scanner.ClassScanner;
import org.live4j.framework.core.scanner.impl.support.AnnotationClassTemplate;
import org.live4j.framework.core.scanner.impl.support.ClassTemplate;
import org.live4j.framework.core.scanner.impl.support.SuperClassTemplate;

import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * Title: 默认的类加载器
 * Description:
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/25 16:23
 */
public class DefaultClassScanner implements ClassScanner {

    /**
     * @param packageName 包名
     * @return 指定包下的所有类
     */
    public Set<Class<?>> getClassSet(String packageName) {
        return new ClassTemplate(packageName){
            @Override
            public boolean checkAddClass(Class<?> cls){
                return true;
            }
        }.getClassSet();
    }

    /**
     * @param packageName 包名
     * @param superClass 父类/接口
     * @return 指定包下的指定父类/接口的所有相关类
     */
    public Set<Class<?>> getClassSetBySuper(String packageName, final Class<?> superClass) {
        return new SuperClassTemplate(packageName, superClass){
            @Override
            public boolean checkAddClass(Class<?> cls){
                return superClass.isAssignableFrom(cls) && !superClass.equals(cls);
            }
        }.getClassSet();
    }

    /**
     * @param packageName 包名
     * @param annotationClass 注解类
     * @return 指定包下的标记指定注解的所有类
     */
    public Set<Class<?>> getClassSetByAnnotation(String packageName, final Class<? extends Annotation> annotationClass) {
        return new AnnotationClassTemplate(packageName, annotationClass){
            @Override
            public boolean checkAddClass(Class<?> cls){
                return cls.isAnnotationPresent(annotationClass);
            }
        }.getClassSet();
    }
}
