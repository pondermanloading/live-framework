package org.live4j.framework.core.scanner;

import org.live4j.framework.core.ConfigHelper;
import org.live4j.framework.core.InstanceFactory;
import org.live4j.framework.core.scanner.ClassScanner;
import org.live4j.framework.dao.annotation.Repository;
import org.live4j.framework.mvc.annotation.Component;
import org.live4j.framework.tx.annotation.Service;
import org.live4j.framework.mvc.annotation.Controller;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

/**
 * Title: 类加载器的助手类
 * Description: 提供获取加载类的相关接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/21 19:09
 */
public final class ClassHelper {
    // 基础包下所有的加载类
    private static final Set<Class<?>> CLASS_SET;

    // 类加载器
    private static ClassScanner classScanner = InstanceFactory.getClassScanner();

    static{
        // 基础包名
        String base_package = ConfigHelper.getAppBasePackage();
        // 基础包下所有的加载类
        CLASS_SET = classScanner.getClassSet(base_package);
    }

    /**
     * 返回基础包下，所有的加载类
     * @return
     */
    public static Set<Class<?>> getClassSet(){
        return CLASS_SET;
    }

    /**
     * 返回基础包下，所有标记注解的加载类(包括@Controller、@Service、@Repository、@Component)
     * @return 注解类
     */
    public static Set<Class<?>> getBeanClassSet(){
        Set<Class<?>> beanClassSet = new HashSet<Class<?>>();
        beanClassSet.addAll(getControllerClassSet());
        beanClassSet.addAll(getServiceClassSet());
        beanClassSet.addAll(getRepositorySet());
        beanClassSet.addAll(getComponentSet());
        return beanClassSet;
    }

    /**
     * 返回基础包下，所有标记指定注解的类
     * @param annotationClass
     * @return
     */
    public static Set<Class<?>> getClassSetByAnnotation(Class<? extends Annotation> annotationClass){
        Set<Class<?>> annotationClassSet = new HashSet<Class<?>>();
        for(Class<?> cls : CLASS_SET){
            if(cls.isAnnotationPresent(annotationClass)){
                annotationClassSet.add(cls);
            }
        }
        return annotationClassSet;
    }

    /**
     * 返回基础包下，指定接口/父类的实现类
     * @param superClass 接口/父类
     * @return 实现类
     */
    public static Set<Class<?>> getClassSetBySuper(Class<?> superClass){
        Set<Class<?>> superClassSet = new HashSet<Class<?>>();
        for(Class<?> cls : CLASS_SET){
            if(superClass.isAssignableFrom(cls) && !superClass.equals(cls)){
                superClassSet.add(cls);
            }
        }
        return superClassSet;
    }

    /**
     * 返回基础包下，所有标记Controller注解类
     * @return
     */
    public static Set<Class<?>> getControllerClassSet(){
        Set<Class<?>> classSet = new HashSet<Class<?>>();
        for(Class<?> cls : CLASS_SET){
            if(cls.isAnnotationPresent(Controller.class)){
                classSet.add(cls);
            }
        }
        return classSet;
    }

    /**
     * 返回基础包下，所有标记Service注解类
     * @return
     */
    public static Set<Class<?>> getServiceClassSet(){
        Set<Class<?>> classSet = new HashSet<Class<?>>();
        for(Class<?> cls : CLASS_SET){
            if(cls.isAnnotationPresent(Service.class)){
                classSet.add(cls);
            }
        }
        return classSet;
    }

    /**
     * 返回基础包下，所有标记@Repository注解的加载类
     * @return
     */
    public static Set<Class<?>> getRepositorySet(){
        Set<Class<?>> classSet = new HashSet<Class<?>>();
        for(Class<?> clz : CLASS_SET){
            if(clz.isAnnotationPresent(Repository.class)){
                classSet.add(clz);
            }
        }
        return classSet;
    }

    /**
     * 返回基础包下，所有标记@Component注解的加载类
     * @return
     */
    public static Set<Class<?>> getComponentSet(){
        Set<Class<?>> classSet = new HashSet<Class<?>>();
        for(Class<?> clz : CLASS_SET){
            if(clz.isAnnotationPresent(Component.class)){
                classSet.add(clz);
            }
        }
        return classSet;
    }

}
