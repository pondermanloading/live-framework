package org.live4j.framework.core.scanner.impl.support;

/**
 * Title: 类加载器的父类/接口模板类
 * Description:
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/25 17:03
 */
public abstract class SuperClassTemplate extends ClassTemplate {
    // 指定父类/接口
    private Class<?> superClass;

    public SuperClassTemplate(String packageName, Class<?> superClass){
        super(packageName);
        this.superClass = superClass;
    }
}
