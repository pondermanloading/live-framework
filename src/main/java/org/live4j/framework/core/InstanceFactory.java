package org.live4j.framework.core;

import org.live4j.framework.core.scanner.ClassScanner;
import org.live4j.framework.core.scanner.impl.DefaultClassScanner;
import org.live4j.framework.dao.DataAccessor;
import org.live4j.framework.dao.impl.DefaultDataAccessor;
import org.live4j.framework.ds.DataSourceDirector;
import org.live4j.framework.ds.builder.DataSourceBuilder;
import org.live4j.framework.ds.builder.impl.DefaultDataSourceBuilder;
import org.live4j.framework.ds.impl.DefaultDataSourceDirector;
import org.live4j.framework.mvc.HandlerExceptionResolver;
import org.live4j.framework.mvc.HandlerAdapter;
import org.live4j.framework.mvc.HandlerMapping;
import org.live4j.framework.mvc.ViewResolver;
import org.live4j.framework.mvc.impl.DefaultHandlerExceptionResolver;
import org.live4j.framework.mvc.impl.DefaultHandlerAdapter;
import org.live4j.framework.mvc.impl.DefaultHandlerMapping;
import org.live4j.framework.mvc.impl.DefaultViewResolver;
import org.live4j.framework.util.ReflectionUtil;
import org.live4j.framework.util.StringUtil;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Title: 超级工厂
 * Description: 提供单例对象的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/29 10:38
 */
public class InstanceFactory {

    private InstanceFactory(){}

    // 缓存器
    private static final Map<String, Object> cache = new ConcurrentHashMap<String, Object>();

    // 处理器映射器
    private static final String HANDLER_MAPPING = "live.framework.custom.handler_mapping";

    // 处理器适配器
    private static final String HANDLER_ADAPTER = "live.framework.custom.handler_adapter";

    // 视图解析器
    private static final String VIEW_RESOLVER = "live.framework.custom.view_resolver";

    // 处理器异常解析器
    private static final String HANDLER_EXCEPTION_RESOLVER = "live.framework.custom.handler_exception_resolver";

    // 类的扫描器
    private static final String CLASS_SCANNER = "live.framework.custom.class_scanner";

    // 数据源指导者
    private static final String DATASOURCE_DIRECTOR = "live.framework.custom.datasource_director";

    // 数据源生成器
    private static final String DATASOURCE_BUILDER = "live.framework.custom.datasource_builder";

    // 数据访问器
    private static final String DATA_ACCESSOR = "live.framework.custom.data_accessor";

    /**
     * 返回 Handler 处理器映射器
     * @return
     */
    public static HandlerMapping getHandlerMapping(){
        return getInstance(HANDLER_MAPPING, DefaultHandlerMapping.class);
    }

    /**
     * 返回视图解析器实例
     * @return
     */
    public static ViewResolver getViewResolver(){
        return getInstance(VIEW_RESOLVER, DefaultViewResolver.class);
    }

    /**
     * 返回 Handler 处理器适配器
     * @return
     */
    public static HandlerAdapter getHandlerAdapter(){
        return getInstance(HANDLER_ADAPTER, DefaultHandlerAdapter.class);
    }

    /**
     * 返回 Handler 处理器异常解析器
     * @return
     */
    public static HandlerExceptionResolver getHandlerExceptionResolver(){
        return getInstance(HANDLER_EXCEPTION_RESOLVER, DefaultHandlerExceptionResolver.class);
    }

    /**
     * 返回数据源指导者
     * @return
     */
    public static DataSourceDirector getDataSourceDirector(){
        return getInstance(DATASOURCE_DIRECTOR, DefaultDataSourceDirector.class);
    }

    /**
     * 返回数据源生成器
     * @return
     */
    public static DataSourceBuilder getDataSourceBuilder(){
        return getInstance(DATASOURCE_BUILDER, DefaultDataSourceBuilder.class);
    }

    /**
     * 返回类的扫描器
     * @return
     */
    public static ClassScanner getClassScanner(){
        return getInstance(CLASS_SCANNER, DefaultClassScanner.class);
    }

    /**
     * 返回数据访问器
     * @return
     */
    public static DataAccessor getDataAccessor(){
        return getInstance(DATA_ACCESSOR, DefaultDataAccessor.class);
    }

    /**
     * 返回指定实例对象
     * @param cacheKey 指定实现
     * @param defaultImplClass 默认实现
     * @param <T> 实例对象
     * @return
     */
    private static <T> T getInstance(String cacheKey, Class<T> defaultImplClass){
        // 判断缓存中是否存在
        if(cache.containsKey(cacheKey)){
            return (T) cache.get(cacheKey);
        }
        // 判断配置中是否配置
        String implClass = ConfigHelper.getString(cacheKey);// 类的完全限定名
        if(StringUtil.isEmpty(implClass)){
            implClass = defaultImplClass.getName();
        }
        // 创建相应实例对象
        T instance = ReflectionUtil.newInstanceByClassName(implClass);
        if(null != instance){
            cache.put(cacheKey, instance);
        }
        return instance;
    }

}
