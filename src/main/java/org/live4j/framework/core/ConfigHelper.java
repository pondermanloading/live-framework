package org.live4j.framework.core;

import org.live4j.framework.util.PropsUtil;

import java.util.Properties;

/**
 * Title:读取系统配置数据
 * Description:提供读取系统配置数据的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/19 20:06
 */
public final class ConfigHelper {

    // 指定配置文件对象
    private static final Properties CONFIG_PROPS = PropsUtil.loadProps(ConfigConstant.CONFIG_FILE);

    private ConfigHelper(){}

    /**
     * 获取JDBC的驱动
     * @return
     */
    public static String getJdbcDriver(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_DRIVER);
    }

    /**
     * 获取JDBC的URL
     * @return
     */
    public static String getJdbcUrl(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_URL);
    }

    /**
     * 获取JDBC的用户名
     * @return
     */
    public static String getJdbcUserName(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_USERNAME);
    }

    /**
     * 获取JDBC的密码
     * @return
     */
    public static String getJdbcPassword(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_PASSWORD);
    }

    /**
     * 数据库连接活跃状态最大连接数
     * @return
     */
    public static Integer getJdbcMaxActive(){
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_MAX_ACTIVE);
    }

    /**
     * 数据库连接活空闲态最大连接数
     * @return
     */
    public static Integer getJdbcMaxIdle(){
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.JDBC_MAX_IDLE);
    }

    /**
     * 获取应用的基础包名
     * @return
     */
    public static String getAppBasePackage(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_BASE_PACKAGE);
    }

    /**
     * 获取应用的JSP基础路径(提供默认值)
     * @return
     */
    public static String getAppJspPath(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_JSP_PATH, "/WEB-INF/view/");
    }

    /**
     * 获取应用的静态资源基础路径(提供默认值)
     * @return
     */
    public static String getAppAssetPath(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_ASSET_PATH, "/asset/");
    }

    /**
     * 获取上传文件大小限制
     * @return
     */
    public static int getAppUploadLimit(){
        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.APP_UPLOAD_LIMIT, 10);
    }

    /**
     * 返回主页
     * @return
     */
    public static String getHomePage(){
        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.HOME_PAGE);
    }

    /**
     * 返回指定视图解析器Class
     * @return
     */
    public static String getString(String key){
        return PropsUtil.getString(CONFIG_PROPS, key);
    }
}
