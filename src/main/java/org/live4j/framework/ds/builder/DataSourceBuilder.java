package org.live4j.framework.ds.builder;

import javax.sql.DataSource;

/**
 * Title:数据源生成器
 * Description:
 * Copyright: 2018
 * Author: Oak
 * Create Time:2018/8/3 20:50
 */
public interface DataSourceBuilder {

    /**
     * @param driver 数据库驱动
     */
    DataSourceBuilder setDriver(String driver);

    /**
     * @param url 数据库URL
     */
    DataSourceBuilder setURL(String url);

    /**
     * @param username 数据库用户名
     */
    DataSourceBuilder setUserName(String username);

    /**
     * @param password 数据库密码
     */
    DataSourceBuilder setPassword(String password);

    /**
     * @param maxActive 活跃状态最大连接数
     */
    DataSourceBuilder setMaxActive(Integer maxActive);

    /**
     * @param maxIdle 空闲状态最大连接数
     */
    DataSourceBuilder setMaxIdle (Integer maxIdle);

    /**
     * @return 数据源对象
     */
    DataSource buildDataSource();
}
