package org.live4j.framework.ds.builder.impl;

import org.apache.commons.dbcp.BasicDataSource;
import org.live4j.framework.ds.builder.DataSourceBuilder;
import org.live4j.framework.util.StringUtil;

import javax.sql.DataSource;

/**
 * Title:默认的数据源生成器
 * Description:
 * Copyright: 2018
 * Author: Oak
 * Create Time:2018/8/3 20:50
 */
public class DefaultDataSourceBuilder implements DataSourceBuilder {
    private BasicDataSource dataSource = new BasicDataSource();

    /**
     * @param driver 数据库驱动
     */
    public DataSourceBuilder setDriver(String driver){
        if(StringUtil.isEmpty(driver))
            throw new RuntimeException("not find database driver！");
        dataSource.setDriverClassName(driver);
        return this;
    }

    /**
     * @param url 数据库URL
     */
    public DataSourceBuilder setURL(String url){
        if(StringUtil.isEmpty(url))
            throw new RuntimeException("not find database url！");
        dataSource.setUrl(url);
        return this;
    }

    /**
     * @param userName 数据库用户名
     */
    public DataSourceBuilder setUserName(String userName){
        if(StringUtil.isEmpty(userName))
            throw new RuntimeException("not find database userName！");
        dataSource.setUsername(userName);
        return this;
    }

    /**
     * @param password 数据库密码
     */
    public DataSourceBuilder setPassword(String password){
        if(StringUtil.isEmpty(password))
            throw new RuntimeException("not find database password！");
        dataSource.setPassword(password);
        return this;
    }

    /**
     * @param maxActive 活跃状态最大连接数
     */
    public DataSourceBuilder setMaxActive(Integer maxActive) {
        if(null != maxActive)
            dataSource.setMaxActive(maxActive);
        dataSource.setMaxActive(maxActive);
        return this;
    }

    /**
     * @param maxIdle 空闲状态最大连接数
     */
    public DataSourceBuilder setMaxIdle(Integer maxIdle) {
        if(null != maxIdle)
            dataSource.setMaxIdle(maxIdle);
        dataSource.setMaxIdle(maxIdle);
        return this;
    }

    /**
     * @return 数据源对象
     */
    public DataSource buildDataSource() {
        return dataSource;
    }
}
