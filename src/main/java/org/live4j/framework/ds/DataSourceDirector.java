package org.live4j.framework.ds;

import javax.sql.DataSource;

/**
 * Title: 数据源指导者
 * Description: 返回数据源
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/19 16:29
 */
public interface DataSourceDirector {
    /**
     * 返回数据源
     * @return
     */
    DataSource getDataSource();
}
