package org.live4j.framework.ds.impl;

import org.live4j.framework.core.ConfigHelper;
import org.live4j.framework.core.InstanceFactory;
import org.live4j.framework.ds.DataSourceDirector;
import org.live4j.framework.ds.builder.DataSourceBuilder;

import javax.sql.DataSource;

/**
 * Title: 抽象数据源指导者
 * Description: 提供返回数据源的接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/20 09:06
 */
public class DefaultDataSourceDirector implements DataSourceDirector{
    private static final String jdbcDriver = ConfigHelper.getJdbcDriver();
    private static final String jdbcUrl = ConfigHelper.getJdbcUrl();
    private static final String userName = ConfigHelper.getJdbcUserName();
    private static final String passWord = ConfigHelper.getJdbcPassword();
    private static final Integer maxActive = ConfigHelper.getJdbcMaxActive();
    private static final Integer maxIdle = ConfigHelper.getJdbcMaxIdle();
    private static final DataSourceBuilder dataSourceBuilder = InstanceFactory.getDataSourceBuilder();

    /**
     * 返回数据源
     * @return
     */
    public final DataSource getDataSource(){
        DataSource dataSource = dataSourceBuilder.setDriver(jdbcDriver)
                .setURL(jdbcUrl)
                .setUserName(userName)
                .setPassword(passWord)
                .setMaxActive(maxActive)
                .setMaxIdle(maxIdle)
                .buildDataSource();
        return dataSource;
    }

}
