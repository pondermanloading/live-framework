package org.live4j.framework.mvc;

import org.live4j.framework.core.ConfigConstant;
import org.live4j.framework.core.HelperLoader;
import org.live4j.framework.core.InstanceFactory;
import org.live4j.framework.core.ConfigHelper;
import org.live4j.framework.mvc.bean.Handler;
import org.live4j.framework.util.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Title: 前端控制器
 * Description: 控制请求流程
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 08:29
 */
@WebServlet(urlPatterns = "/*", loadOnStartup = 0)
public class DispatcherServlet extends HttpServlet{
    // 处理器映射器
    private static final HandlerMapping HANDLER_MAPPING = InstanceFactory.getHandlerMapping();

    // 处理器适配器
    private static final HandlerAdapter HANDLER_ADAPTER = InstanceFactory.getHandlerAdapter();

    // 视图解析器
    private static final ViewResolver VIEW_RESOLVER = InstanceFactory.getViewResolver();

    // 处理器异常解析器
    private static final HandlerExceptionResolver HANDLER_EXCEPTION_RESOLVER = InstanceFactory.getHandlerExceptionResolver();

    /**
     * 初始化方法
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException{
        // 加载Helper类
        HelperLoader.init();
        // 返回ServletContext(注册Servlet)
        ServletContext context = config.getServletContext();
        // 注册处理Jsp的Servlet，将所有jsp请求注册到JspServlet  ？？有什么用？
        ServletRegistration jspServlet = context.getServletRegistration("jsp");
        jspServlet.addMapping(ConfigHelper.getAppJspPath() + "*");
        // 注册处理静态资源的Servlet  ？？有什么用？
        // Web应用服务器默认的Servlet名称是"default"，指定静态资源路径下的资源由它处理
        ServletRegistration defaultServlet = context.getServletRegistration("default");
        defaultServlet.addMapping(ConfigHelper.getAppAssetPath() + "*");
        // 初始化文件操作
        UploadHelper.init(context);
    }

    /**
     * 服务方法
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{
        // 设置编码 UTF-8
        request.setCharacterEncoding(ConfigConstant.DEFAULT_CODE);
        // 请求方法，小写
        String requestMethod = request.getMethod().toLowerCase();
        // 请求路径
        String requestPath = WebUtil.getRequestPath(request);
        // 跳过这个请求
        if("/favicon.ico".equals(requestPath)){
            return;
        }
        // 如果请求路径等于'/'，重定向到主页
        if("/".equals(requestPath)){
            WebUtil.redirectRequest(request, response, ConfigHelper.getHomePage());
            return;
        }
        // 如果请求路径以'/'结尾
        if(requestPath.endsWith("/")){
            requestPath = requestPath.substring(0, requestPath.lastIndexOf("/"));
        }
        // 处理器
        Handler handler = HANDLER_MAPPING.getHandler(requestMethod, requestPath);
        if(null == handler){// 404页面
            WebUtil.sendError(HttpServletResponse.SC_NOT_FOUND, "", response);
            return;
        }
        // 初始化上下文
        DataContext.init(request, response);
        try {
            // 调用适配器，并进行视图解析
            Object modelAndView = HANDLER_ADAPTER.adapteHandler(request, response, handler);
            // 视图解析器
            VIEW_RESOLVER.resolveView(modelAndView);
        }catch(Exception ex){
            // 异常处理
            HANDLER_EXCEPTION_RESOLVER.resolveHandlerException(request, response, ex);
        }finally{
            // 销毁上下文
            DataContext.destory();
        }
    }
}
