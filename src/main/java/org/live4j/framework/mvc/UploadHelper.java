package org.live4j.framework.mvc;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.live4j.framework.core.ConfigConstant;
import org.live4j.framework.core.InstanceFactory;
import org.live4j.framework.mvc.bean.FileParam;
import org.live4j.framework.mvc.bean.FormParam;
import org.live4j.framework.mvc.bean.Param;
import org.live4j.framework.core.ConfigHelper;
import org.live4j.framework.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Title: 上传文件帮助类
 * Description: 封装上传文件相关接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/25 20:12
 */
public class UploadHelper {
    private static final Logger logger = LoggerFactory.getLogger(UploadHelper.class);
    // 操作文件对象
    private static ServletFileUpload servletFileUpload;

    /**
     * 初始化文件操作类
     * @param servletContext
     */
    public static void init(ServletContext servletContext){
        // 服务器临时文件目录(Tomcat的work目录)
        File repository = (File)servletContext.getAttribute("javax.servlet.context.tempdir");
        // 设置内部缓冲区大小和临时文件目录
        servletFileUpload = new ServletFileUpload(new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, repository));
        // 设置文件大小限制，默认10
        int uploadLimit = ConfigHelper.getAppUploadLimit();
        if(0 != uploadLimit){
            servletFileUpload.setFileSizeMax(uploadLimit * 1024 * 1024);
        }
    }

    /**
     * 返回请求参数
     * @param request 请求
     * @return 请求参数
     */
    public static Param createParam(HttpServletRequest request){
        // 表单参数
        List<FormParam> formParamList = new ArrayList<FormParam>();
        // 文件参数
        List<FileParam> fileParamList = new ArrayList<FileParam>();
        try{
            // List<FileItem> fileItemList = servletFileUpload.parseRequest(request); 也行，不过从源码来看处理效率低
            Map<String, List<FileItem>> fileItemListMap = servletFileUpload.parseParameterMap(request);
            if(MapUtil.isNotEmpty(fileItemListMap)){
                for(Map.Entry<String, List<FileItem>> entry : fileItemListMap.entrySet()){
                    // 表单属性字段名
                    String fieldName = entry.getKey();
                    // 当前表单属性字段名下的所有表单属性项
                    List<FileItem> fileItemList = entry.getValue();
                    if(CollectionUtil.isNotEmpty(fileItemList)){
                        for(FileItem fileItem : fileItemList){
                            if(fileItem.isFormField()){// 普通表单属性
                                // 表单属性字段值
                                String fieldValue = fileItem.getString(ConfigConstant.DEFAULT_CODE);
                                formParamList.add(new FormParam(fieldName, fieldValue));
                            }else{// 文件表单属性
                                // 文件名
                                String fileName = FileUtil.getRealFileName(new String(fileItem.getName().getBytes(), ConfigConstant.DEFAULT_CODE));
                                if(StringUtil.isNotEmpty(fileName)){
                                    // 文件大小
                                    long fileSize = fileItem.getSize();
                                    // 文件类型
                                    String contentType = fileItem.getContentType();
                                    // 文件流
                                    InputStream inputStream = fileItem.getInputStream();
                                    fileParamList.add(new FileParam(fieldName, fileName, fileSize, contentType, inputStream));
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception ex){
            logger.error("封装Param对象异常" + ex);
            throw new RuntimeException("封装Param对象异常" + ex);
        }
        return new Param(formParamList, fileParamList);
    }

    /**
     * 上传单个文件
     * @param basePath 指定文件目录
     * @param fileParam 单个文件参数
     */
    public static void uploadFile(String basePath, FileParam fileParam){
        try {
            if(null != fileParam){
                // 文件完整路径
                String filePath = basePath + fileParam.getFileName();
                // 是否创建文件所在目录
                FileUtil.createFile(filePath);
                // 输入流
                InputStream inputStream = new BufferedInputStream(fileParam.getInputStream());
                // 输出流
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(filePath));
                StreamUtil.copyStream(inputStream, outputStream);
            }
        } catch (Exception ex) {
            logger.error("上传文件失败" + ex);
            throw new RuntimeException("上传文件失败" + ex);
        }
    }

    /**
     * 批量上传文件
     * @param basePath 上传文件目录
     * @param fileParamList 上传文件
     */
    public static void uploadFile(String basePath, List<FileParam> fileParamList){
        try{
            if(CollectionUtil.isNotEmpty(fileParamList)){
                for(FileParam fileParam : fileParamList){
                    uploadFile(basePath, fileParam);
                }
            }
        }catch(Exception ex){
            logger.error("批量上传文件失败" + ex);
            throw new RuntimeException("批量上传文件失败" + ex);
        }
    }

    /**
     * 判断是否是multipart/form-data二进制流
     * @param request
     * @return
     */
    public static boolean isMultipart(HttpServletRequest request){
        return ServletFileUpload.isMultipartContent(request);
    }

}
