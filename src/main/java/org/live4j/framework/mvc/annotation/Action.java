package org.live4j.framework.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Title: Action方法注解
 * Description: 标识Action方法
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/21 19:00
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {
    // 请求方法和请求路径
    String value();
}
