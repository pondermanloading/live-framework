package org.live4j.framework.mvc;

import org.live4j.framework.mvc.bean.Handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Title: 适配器
 * Description: 调用Handler
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 08:15
 */
public interface HandlerAdapter {

    /**
     * 调用Handler
     * @param request
     * @param response
     * @param handler
     */
    Object adapteHandler(HttpServletRequest request, HttpServletResponse response, Handler handler);
}
