package org.live4j.framework.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Title: 异常处理器
 * Description: 调用处理器时的异常
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 10:57
 */
public interface HandlerExceptionResolver {

    /**
     * 处理器异常
     * @param request 请求
     * @param response 响应
     * @param ex 异常
     */
    void resolveHandlerException(HttpServletRequest request, HttpServletResponse response, Exception ex);
}
