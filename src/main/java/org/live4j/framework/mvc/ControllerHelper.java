package org.live4j.framework.mvc;

import org.live4j.framework.core.scanner.ClassHelper;
import org.live4j.framework.mvc.annotation.Action;
import org.live4j.framework.mvc.bean.Handler;
import org.live4j.framework.mvc.bean.Request;
import org.live4j.framework.util.ArrayUtil;
import org.live4j.framework.util.CollectionUtil;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Title: 控制器映射信息
 * Description: 初始化请求与处理器的映射关系
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 10:35
 */
public class ControllerHelper {
    // 请求与处理器映射
    private static final Map<Request, Handler> ACTION_MAP = new HashMap<Request, Handler>();

    static{
        Set<Class<?>> controllerClassSet = ClassHelper.getControllerClassSet();// 返回所有Controller的注解类
        if(CollectionUtil.isNotEmpty(controllerClassSet)){
            for(Class<?> controllerClass : controllerClassSet){
                Method[] methods = controllerClass.getDeclaredMethods();// 当前Controller类的所有方法对象
                if(ArrayUtil.isNotEmpty(methods)){
                    for(Method method : methods){
                        if(method.isAnnotationPresent(Action.class)){// 判断当前对象是否存在@Action
                            Action action = method.getAnnotation(Action.class);// 返回当前方法对象的@Action对象
                            String actionValue = action.value();// 返回@Action对象的值，如get:/addUser
                            if(actionValue.matches("\\w+:/\\w*")){// 满足匹配原则
                                String[] params = actionValue.split(":");
                                if(ArrayUtil.isNotEmpty(params) && 2 == params.length){
                                    String requestMethod = params[0];// 请求方法，get或post
                                    String requestPath = params[1];// 请求路径 /addUser
                                    Request request = new Request(requestMethod, requestPath);
                                    Handler handler = new Handler(controllerClass, method);
                                    ACTION_MAP.put(request, handler);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 返回请求和处理器映射
     * @return
     */
    public static Map<Request, Handler> getRequestHandlerMap(){
        return ACTION_MAP;
    }
}
