package org.live4j.framework.mvc.bean;

import java.lang.reflect.Method;
import java.util.regex.Matcher;

/**
 * Title: 处理器
 * Description: 封装Action信息
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 10:21
 */
public class Handler {
    // Controller的Class类
    private Class<?> controllerClass;
    // Method对象
    private Method actionMethod;

    /**
     * 构造器
     * @param controllerClass Class类
     * @param actionMethod 方法对象
     */
    public Handler(Class<?> controllerClass, Method actionMethod) {
        this.controllerClass = controllerClass;
        this.actionMethod = actionMethod;
    }

    public Class<?> getControllerClass() {
        return controllerClass;
    }

    public void setControllerClass(Class<?> controllerClass) {
        this.controllerClass = controllerClass;
    }

    public Method getActionMethod() {
        return actionMethod;
    }

    public void setActionMethod(Method actionMethod) {
        this.actionMethod = actionMethod;
    }

}
