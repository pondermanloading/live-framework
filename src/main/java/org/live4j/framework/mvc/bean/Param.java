package org.live4j.framework.mvc.bean;

import org.live4j.framework.util.CastUtil;
import org.live4j.framework.util.CollectionUtil;
import org.live4j.framework.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Title: 参数类
 * Description: 封装请求参数
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 08:06
 */
public class Param {
    // 表单参数
    private List<FormParam> formParamList;
    // 文件参数
    private List<FileParam> fileParamList;

    public Param(List<FormParam> formParamList) {
        this.formParamList = formParamList;
    }

    public Param(List<FormParam> formParamList, List<FileParam> fileParamList) {
        this.formParamList = formParamList;
        this.fileParamList = fileParamList;
    }

    /**
     * 返回表单参数
     * @return 表单参数名和参数值的映射
     */
    public Map<String, Object> getFormMap(){
        Map<String, Object> formMap = new HashMap<String, Object>();
        if(CollectionUtil.isNotEmpty(formParamList)){
            for(FormParam formParam : formParamList){
                // 参数名
                String fieldName = formParam.getFieldName();
                // 参数值
                Object fieldValue = formParam.getFieldValue();
                if(formMap.containsKey(fieldName)){
                    fieldValue = formMap.get(fieldName)+ StringUtil.SEPARATOR + fieldValue;
                }
                formMap.put(fieldName, fieldValue);
            }
        }
        return formMap;
    }

    /**
     * 返回文件参数
     * @return 文件表单字段名fieldName和文件参数映射 List<FileParam>
     */
    public Map<String, List<FileParam>> getFileMap(){
        Map<String, List<FileParam>> fileMap = new HashMap<String, List<FileParam>>();
        if(CollectionUtil.isNotEmpty(fileParamList)){
            for(FileParam fileParam : fileParamList){
                // 文件表单字段名
                String fieldName = fileParam.getFieldName();
                List<FileParam> fileParamListTmp;
                if(fileMap.containsKey(fieldName)){
                    fileParamListTmp = fileMap.get(fieldName);
                }else{
                    fileParamListTmp = new ArrayList<FileParam>();
                }
                fileParamListTmp.add(fileParam);
                fileMap.put(fieldName, fileParamListTmp);
            }
        }
        return fileMap;
    }

    /**
     * 返回所有文件
     * @param fieldName 文件表单字段名
     * @return 所有文件
     */
    public List<FileParam> getFileList(String fieldName){
        return getFileMap().get(fieldName);
    }

    /**
     * 返回单个文件
     * @param fieldName 文件表单字段名
     * @return 单个文件
     */
    public FileParam getFile(String fieldName){
        // 指定文件表单字段名下的所有的文件
        List<FileParam> fileParamTmp = getFileMap().get(fieldName);
        if(CollectionUtil.isNotEmpty(fileParamTmp) && 1 == fileParamTmp.size()){
            return fileParamTmp.get(0);
        }
        return null;
    }

    /**
     * 判断当前Param是否为空
     * @return 布尔值
     */
    public boolean isEmpty(){
        return CollectionUtil.isNotEmpty(formParamList) && CollectionUtil.isNotEmpty(fileParamList);
    }

    /**
     * 返回String类型的表单参数值
     * @param fieldName 表单参数名
     * @return
     */
    public String getString(String fieldName){
        return CastUtil.castString(getFormMap().get(fieldName));
    }

    /**
     * 返回int类型的表单参数值
     * @param fieldName 表单参数名
     * @return
     */
    public int getInt(String fieldName){
        return CastUtil.castInt(getFormMap().get(fieldName));
    }

    /**
     * 返回double类型的表单参数值
     * @param fieldName 表单参数名
     * @return
     */
    public Double getDouble(String fieldName){
        return CastUtil.castDouble(getFormMap().get(fieldName));
    }
}
