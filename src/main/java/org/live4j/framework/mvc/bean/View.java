package org.live4j.framework.mvc.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * Title: 视图
 * Description: 封装视图信息
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 08:16
 */
public class View {
    // 视图路径
    private String path;
    // 模型
    private Map<String, Object> model;

    /**
     * 构造器
     * @param path 视图路径
     */
    public View(String path) {
        this.path = path;
        model = new HashMap<String, Object>();
    }

    /**
     * 添加模型
     * @param model 模型
     * @return 视图
     */
    public View addModel(Map<String, Object> model){
        this.model = model;
        return this;
    }

    /**
     * 判断是否是服务器根目录
     * @param path
     * @return
     */
    public boolean isRedirect(String path){
        return path.startsWith("/");
    }

    public String getPath() {
        return path;
    }

    public Map<String, Object> getModel() {
        return model;
    }
}
