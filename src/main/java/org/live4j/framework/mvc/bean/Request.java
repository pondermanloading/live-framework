package org.live4j.framework.mvc.bean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Title: 请求类
 * Description: 封装请求信息
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/22 10:20
 */
public class Request {
    // 请求方法
    private String requestMethod;
    // 请求路径
    private String requestPath;

    /**
     * 构造器
     * @param requestMehod 请求方法
     * @param requestPath 请求路径
     */
    public Request(String requestMehod, String requestPath){
        this.requestMethod = requestMethod;
        this.requestPath = requestPath;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    @Override
    public int hashCode(){
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj){
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
