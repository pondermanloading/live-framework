package org.live4j.framework.mvc.bean;

/**
 * Title: 返回前端数据
 * Description: 用于转化Json格式的数据
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/23 08:18
 */
public class Data {
    private Object model;

    public Data(Object model) {
        this.model = model;
    }

    public Object getModel() {
        return model;
    }
}
