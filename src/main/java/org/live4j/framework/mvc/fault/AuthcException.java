package org.live4j.framework.mvc.fault;

/**
 * Title: 认证异常（非法访问异常）
 * Description: 访问非法地址时的异常
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 10:53
 */
public class AuthcException extends RuntimeException {

    public AuthcException(){ super();}

    public AuthcException(String msg){
        super(msg);
    }

    public AuthcException(String msg, Throwable throwable){
        super(msg, throwable);
    }

    public AuthcException(Throwable throwable){
        super(throwable);
    }
}
