package org.live4j.framework.mvc.fault;

/**
 * Title: 权限异常
 * Description: 无权访问异常
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 10:55
 */
public class AuthzException extends RuntimeException {
    public AuthzException(){ super();}

    public AuthzException(String msg){
        super(msg);
    }

    public AuthzException(String msg, Throwable throwable){
        super(msg, throwable);
    }

    public AuthzException(Throwable throwable){
        super(throwable);
    }
}
