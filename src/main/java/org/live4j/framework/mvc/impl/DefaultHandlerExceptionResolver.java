package org.live4j.framework.mvc.impl;

import org.live4j.framework.core.ConfigHelper;
import org.live4j.framework.mvc.HandlerExceptionResolver;
import org.live4j.framework.mvc.fault.AuthcException;
import org.live4j.framework.mvc.fault.AuthzException;
import org.live4j.framework.util.WebUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Title: 异常处理器
 * Description: 调用处理器时的异常
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 11:01
 */
public class DefaultHandlerExceptionResolver implements HandlerExceptionResolver {
    private static final Logger logger = LoggerFactory.getLogger(DefaultHandlerExceptionResolver.class);

    /**
     * 处理器异常
     * @param request 请求
     * @param response 响应
     * @param ex 异常
     */
    public void resolveHandlerException(HttpServletRequest request, HttpServletResponse response, Exception ex){
        // 异常类型
        Throwable throwable = ex.getCause();
        if(null == throwable){
            logger.error(ex.getMessage(), ex);
        }
        if(throwable instanceof AuthcException){// 认证异常
            if(WebUtil.isAjax(request)){// 403
                WebUtil.sendError(HttpServletResponse.SC_FORBIDDEN, "", response);
            }else{// 主页
                WebUtil.redirectRequest(request, response, ConfigHelper.getHomePage());
            }
        }else if(throwable instanceof AuthzException){// 权限异常 403
            WebUtil.sendError(HttpServletResponse.SC_FORBIDDEN, "", response);
        }else{// 500
            WebUtil.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage(), response);
        }
    }
}
