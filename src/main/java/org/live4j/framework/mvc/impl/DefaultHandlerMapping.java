package org.live4j.framework.mvc.impl;

import org.live4j.framework.mvc.ControllerHelper;
import org.live4j.framework.mvc.HandlerMapping;
import org.live4j.framework.mvc.bean.Handler;
import org.live4j.framework.mvc.bean.Request;
import org.live4j.framework.util.MapUtil;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Title: 处理器映射器实现
 * Description: 返回Handler处理器
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 08:00
 */
public class DefaultHandlerMapping implements HandlerMapping{

    /**
     * 返回Handler处理器
     * @param requestMethod 请求方法
     * @param requestPath 请求路径
     * @return
     */
    public Handler getHandler(String requestMethod, String requestPath){
        // 映射关系
        Map<Request, Handler> actionMap = ControllerHelper.getRequestHandlerMap();
        Request request = new Request(requestMethod, requestPath);
        return actionMap.get(request);
    }
}
