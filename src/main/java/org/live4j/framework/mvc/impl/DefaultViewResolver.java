package org.live4j.framework.mvc.impl;

import org.live4j.framework.mvc.DataContext;
import org.live4j.framework.mvc.bean.Data;
import org.live4j.framework.mvc.bean.View;
import org.live4j.framework.mvc.UploadHelper;
import org.live4j.framework.mvc.ViewResolver;
import org.live4j.framework.util.WebUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Title: 视图解析器
 * Description: 根据返回视图进行相应的处理
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/26 10:25
 */
public class DefaultViewResolver implements ViewResolver{

    /**
     * 解析视图
     * @param modelAndView 数据视图
     */
    public void resolveView(Object modelAndView){
        // 请求
        HttpServletRequest request = DataContext.getRequest();
        // 响应
        HttpServletResponse response = DataContext.getResponse();
        // 判断视图类型 View 或 Data
        if(modelAndView instanceof View){
            handleViewResult((View) modelAndView, request, response);
        }else if(modelAndView instanceof Data){
            handleDataResult((Data) modelAndView, request, response);
        }
    }

    /**
     * 处理视图
     * @param view 视图
     * @param request 请求
     * @param response 响应
     */
    private void handleViewResult(View view, HttpServletRequest request ,HttpServletResponse response){
        // 路径
        String path = view.getPath();
        if(view.isRedirect(path)){// 以服务器的根目录开始
            WebUtil.redirectRequest(request, response, path);
        }else{// 以当前应用的根目录开始
            // 数据
            Map<String, Object> modelMap = view.getModel();
            for(Map.Entry<String, Object> entry : modelMap.entrySet()){
                // 参数名
                String modelName = entry.getKey();
                // 参数值
                Object modelValue = entry.getValue();
                request.setAttribute(modelName, modelValue);
            }
            WebUtil.forwardRequest(request, response, path);
        }
    }

    /**
     * 处理Data视图
     * @param data
     */
    private void handleDataResult(Data data, HttpServletRequest request ,HttpServletResponse response){
        Object model = data.getModel();
        if(null != model) {
            if(UploadHelper.isMultipart(request)){// 上传文件，响应返回Json字符串（HTML格式）
                WebUtil.writeHTML(response, model);
            }else{// 普通参数，响应返回Json字符串（JSON格式）
                WebUtil.writeJson(response, model);
            }
        }
    }
}
