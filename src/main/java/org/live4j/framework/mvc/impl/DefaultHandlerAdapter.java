package org.live4j.framework.mvc.impl;

import org.live4j.framework.ioc.BeanHelper;
import org.live4j.framework.mvc.HandlerAdapter;
import org.live4j.framework.mvc.RequestHelper;
import org.live4j.framework.mvc.UploadHelper;
import org.live4j.framework.mvc.bean.Handler;
import org.live4j.framework.mvc.bean.Param;
import org.live4j.framework.util.ReflectionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * Title: 默认的处理器适配器
 * Description: 调用handler
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 09:51
 */
public class DefaultHandlerAdapter implements HandlerAdapter {

    /**
     * 调用Handler
     * @param request 请求
     * @param response 响应
     * @param handler 处理器
     * @return 数据视图
     */
    public Object adapteHandler(HttpServletRequest request, HttpServletResponse response, Handler handler) {
        // Controller的加载类
        Class<?> controllerClass = handler.getControllerClass();
        Object controllerInstance = BeanHelper.getBean(controllerClass);
        // Controller的Method方法对象
        Method method = handler.getActionMethod();
        // 请求参数
        Param param = getActionParam(request);
        // 方法返回值
        Object actionResult = getActionResult(controllerInstance, method, param);
        // 数据视图
        return actionResult;
    }

    /**
     * 返回请求参数
     * @param request 请求
     * @return 请求参数
     */
    private Param getActionParam(HttpServletRequest request){
        if(UploadHelper.isMultipart(request)){// 请求参数是mulitpart/form-data编码的上传文件
            return UploadHelper.createParam(request);
        }else{// 请求参数是application/x-www-form-urlencoded编码
            return RequestHelper.createParam(request);
        }
    }

    /**
     * 返回执行方法返回值
     * @param controllerInstance Controller对象
     * @param method 方法对象
     * @param param 请求参数
     * @return
     */
    private Object getActionResult(Object controllerInstance, Method method, Param param){
        if(param.isEmpty()){// 请求参数为空
            return ReflectionUtil.invokeMethod(controllerInstance, method);
        }else{
            return ReflectionUtil.invokeMethod(controllerInstance, method, param);
        }
    }


}
