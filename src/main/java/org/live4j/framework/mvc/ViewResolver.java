package org.live4j.framework.mvc;

/**
 * Title: 视图解析器
 * Description: 封装视图解析接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/29 10:09
 */
public interface ViewResolver {

    /**
     * 解析视图
     * @param modelAndView 数据视图
     */
    void resolveView(Object modelAndView);
}
