package org.live4j.framework.mvc;

import org.live4j.framework.mvc.bean.Handler;

/**
 * Title: 处理器映射
 * Description: 返回Handler处理器
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/30 07:58
 */
public interface HandlerMapping {

    /**
     * 返回指定Handler
     * @param requestMethod 请求方法
     * @param requestPath 请求路径
     * @return Handler
     */
    Handler getHandler(String requestMethod, String requestPath);
}
