package org.live4j.framework.mvc;

import org.live4j.framework.mvc.bean.FormParam;
import org.live4j.framework.mvc.bean.Param;
import org.live4j.framework.util.ArrayUtil;
import org.live4j.framework.util.CodeUtil;
import org.live4j.framework.util.StreamUtil;
import org.live4j.framework.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Title: 请求参数助手类
 * Description: 提供返回请求参数接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/26 09:44
 */
public final class RequestHelper {
    private static final Logger logger = LoggerFactory.getLogger(RequestHelper.class);

    /**
     * 返回application/x-www-form-urlencoded编码的请求参数
     * @param request
     * @return
     */
    public static Param createParam(HttpServletRequest request) {
        // 请求参数
        List<FormParam> formParamList = new ArrayList<FormParam>();
        formParamList.addAll(parseParameterNames(request));
        formParamList.addAll(parseInputStream(request));
        return new Param(formParamList);
    }

    /**
     * 解析post请求参数
     * @param request
     * @return post请求参数
     */
    private static List<FormParam> parseParameterNames(HttpServletRequest request){
        List<FormParam> formParamList = new ArrayList<FormParam>();
        Enumeration<String> paramNames = request.getParameterNames();// 所有参数名
        while(paramNames.hasMoreElements()){
            String paramName = paramNames.nextElement();// 参数名
            String[] paramValues = request.getParameterValues(paramName);// 当前参数名下的所有参数值
            if(ArrayUtil.isNotEmpty(paramValues)){
                String paramValue;// 参数值
                if(1 == paramValues.length){// 参数值就一个
                    paramValue = paramValues[0];
                }else{
                    StringBuilder builder = new StringBuilder("");
                    for(String value : paramValues){// 拼接参数值
                        builder.append(value + StringUtil.SEPARATOR);
                    }
                    builder.setLength(builder.length()-1);
                    paramValue = builder.toString();
                }
                formParamList.add(new FormParam(paramName, paramValue));
            }
        }
        return formParamList;
    }

    /**
     * 解析get请求参数
     * @param request
     * @return get请求参数
     */
    private static List<FormParam> parseInputStream(HttpServletRequest request){
        List<FormParam> formParamList = new ArrayList<FormParam>();
        // 请求参数
        String body = null;
        try {
            body = CodeUtil.decodeURL(StreamUtil.getString(request.getInputStream()));
        } catch (IOException ex) {
            logger.error("获取get请求参数流异常" + ex);
        }
        if(StringUtil.isNotEmpty(body)){
            String[] kvs = StringUtil.splitString(body, "&");
            if(ArrayUtil.isNotEmpty(kvs)){
                for(String kv : kvs){
                    String[] array = StringUtil.splitString(kv, "=");
                    if(ArrayUtil.isNotEmpty(array) && 2 == array.length){
                        // 参数名
                        String paramName = array[0];
                        // 参数值
                        String paramValue = array[1];
                        formParamList.add(new FormParam(paramName, paramValue));
                    }
                }
            }
        }
        return formParamList;
    }
}
