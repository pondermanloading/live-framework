package org.live4j.framework.mvc;

import org.live4j.framework.util.ArrayUtil;
import org.live4j.framework.util.CastUtil;
import org.live4j.framework.util.CodeUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Title: 上下文数据
 * Description: 封装上下文数据接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/3 09:55
 */
public class DataContext {
    // 本地线程
    private static final ThreadLocal<DataContext> dataContainer = new ThreadLocal<DataContext>();
    // 请求
    private HttpServletRequest request;
    // 响应
    private HttpServletResponse response;

    /**
     * 构造器
     * @param request
     * @param response
     */
    public DataContext(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    }

    /**
     * 初始化上下文
     * @param request
     * @param response
     */
    public static void init(HttpServletRequest request, HttpServletResponse response){
        dataContainer.set(new DataContext(request, response));
    }

    /**
     * 销毁上下文
     */
    public static void destory(){
        dataContainer.remove();
    }

    /**
     * 返回上下文
     * @return
     */
    private static DataContext getInstance(){
        return dataContainer.get();
    }

    /**
     * 返回request
     * @return
     */
    public static HttpServletRequest getRequest(){
        return getInstance().request;
    }

    /**
     * 返回response
     * @return
     */
    public static HttpServletResponse getResponse(){
        return getInstance().response;
    }

    /**
     * 返回session
     * @return
     */
    public static HttpSession getSession(){
        return getRequest().getSession();
    }

    /**
     * 返回servletContext
     * @return
     */
    public static javax.servlet.ServletContext getServletContext(){
        return getRequest().getServletContext();
    }

    /**
     * request类级内部类
     */
    public static class Request{

        /**
         * 设置值
         * @param key
         * @param value
         */
        public static void put(String key, Object value){
            getRequest().setAttribute(key, value);
        }

        /**
         * 返回指定的值
         * @param key
         * @return
         */
        public static <T> T get(String key){
            return (T) getRequest().getAttribute(key);
        }

        /**
         * 移除指定映射
         * @param key
         */
        public static void remove(String key){
            getRequest().removeAttribute(key);
        }

        /**
         * 返回所有映射关系
         * @return
         */
        public static Map<String, Object> getAll(){
            HttpServletRequest request = getRequest();
            Map<String, Object> map = new HashMap<String, Object>();
            // 所有的请求参数名
            Enumeration<String> paramNames = request.getParameterNames();
            while(paramNames.hasMoreElements()){
                // 请求参数名
                String paramName = paramNames.nextElement();
                // 请求参数值
                String paramValue = request.getParameter(paramName);
                map.put(paramName, paramValue);
            }
            return map;
        }
    }

    /**
     * response类级内部类
     */
    public static class Response{
        /**
         * 设置头信息
         * @param key
         * @param value
         */
        public static void put(String key, String value){
            getResponse().setHeader(key, value);
        }

        /**
         * 返回指定头信息
         * @param key
         * @return
         */
        public static <T> T get(String key){
            return (T) getResponse().getHeader(key);
        }

        /**
         * 返回所有头信息映射
         * @return
         */
        public static Map<String, Object> getAll(){
            Map<String, Object> map = new HashMap<String, Object>();
            for(String key : getResponse().getHeaderNames()){
                map.put(key,getResponse().getHeader(key));
            }
            return map;
        }
    }

    /**
     * session类级内部类
     */
    public static class Session{
        /**
         * 设置session映射
         * @param key
         * @param value
         */
        public static void put(String key, Object value){
            getSession().setAttribute(key, value);
        }

        /**
         * 返回指定session
         * @param key
         * @param <T>
         * @return
         */
        public static <T> T get(String key){
            return (T) getSession().getAttribute(key);
        }

        /**
         * 移除指定session
         * @param key
         */
        public static void remove(String key){
            getSession().removeAttribute(key);
        }

        /**
         * 返回session所有的数据
         * @return
         */
        public static Map<String, Object> getAll(){
            HttpSession session = getSession();
            Map<String, Object> map = new HashMap<String, Object>();
            Enumeration<String> names = session.getAttributeNames();
            while(names.hasMoreElements()){
                String name = names.nextElement();
                Object value = session.getAttribute(name);
                map.put(name, value);
            }
            return map;
        }
    }

    /**
     * ServletContext类级内部类
     */
    public static class ServletContext{
        /**
         * 设置值
         * @param key
         * @param value
         */
        public static void put(String key, Object value){
            getServletContext().setAttribute(key, value);
        }

        /**
         * 返回指定servletContext
         * @param key
         * @param <T>
         * @return
         */
        public static <T> T get(String key){
            return (T) getServletContext().getAttribute(key);
        }

        /**
         * 移除指定servletContext
         * @param key
         */
        public static void remove(String key){
            getServletContext().removeAttribute(key);
        }

        public static Map<String, Object> getAll(){
            javax.servlet.ServletContext servletContext = getServletContext();
            Map<String, Object> map = new HashMap<String, Object>();
            Enumeration<String> names = servletContext.getInitParameterNames();
            while(names.hasMoreElements()){
                String name = names.nextElement();
                Object value = servletContext.getInitParameter(name);
                map.put(name, value);
            }
            return map;
        }
    }

    /**
     * cookie类级内部类
     */
    public static class Cookie{

        /**
         * 存入cookie
         * @param key
         * @param value
         */
        public static void put(String key, Object value){
            String newValue = CodeUtil.encodeURL(CastUtil.castString(value));
            javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(key, newValue);
            getResponse().addCookie(cookie);
        }


        /**
         * 取出cookie中指定key的值
         * @param key
         * @return
         */
        public static <T> T get(String key){
            T value = null;
            javax.servlet.http.Cookie[] cookies = getRequest().getCookies();
            if(ArrayUtil.isNotEmpty(cookies)){
                for(javax.servlet.http.Cookie cookie : cookies){
                    if(key.equals(cookie.getName())){
                        value = (T) CodeUtil.decodeURL(cookie.getValue());
                        break;
                    }
                }
            }
            return value;
        }

        /**
         * 返回cookie的所有值
         * @return
         */
        public static Map<String, Object> getAll(){
            Map<String, Object> map = new HashMap<String, Object>();
            javax.servlet.http.Cookie[] cookies = getRequest().getCookies();
            if(ArrayUtil.isNotEmpty(cookies)){
                for(javax.servlet.http.Cookie cookie : cookies){
                    map.put(cookie.getName(), CodeUtil.decodeURL(cookie.getValue()));
                }
            }
            return map;
        }
    }

}
