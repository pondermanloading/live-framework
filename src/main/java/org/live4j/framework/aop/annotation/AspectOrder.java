package org.live4j.framework.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Title: 切面排序号
 * Description:序号小的在前面
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/26 08:20
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AspectOrder {
    /**
     * 排序号
     * @return
     */
    int value();
}
