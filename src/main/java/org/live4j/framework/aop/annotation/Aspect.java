package org.live4j.framework.aop.annotation;

import java.lang.annotation.*;

/**
 * Title: 切面注解
 * Description: 标记切面类
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/8 08:06
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {

    /**
     * @return 指定包
     */
    String pk();

    /**
     * @return 指定类
     */
    String cls();

    /**
     * @return 指定注解
     */
    Class<? extends Annotation> annotation() default Aspect.class;
}
