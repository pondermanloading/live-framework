package org.live4j.framework.aop;


import org.live4j.framework.aop.proxy.Proxy;
import org.live4j.framework.aop.proxy.ProxyChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * Title: 切面类的抽象类
 * Description: 抽取增强方法的公用算法步骤，定义个性化的抽象方法（增强方法）
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/8 08:40
 */
public abstract class AspectProxy implements Proxy {
    private static final Logger logger = LoggerFactory.getLogger(AspectProxy.class);

    /**
     * 执行增强方法
     * @param proxyChain 代理链
     * @return
     */
    public Object doProxy(ProxyChain proxyChain) throws Throwable{
        Object result;
        // 目标对象的方法对象
        Method targetMethod = proxyChain.getTargetMethod();
        // 目标对象的方法参数
        Object[] methodParams = proxyChain.getMethodParams();
        // 目标加载类
        Class<?> targetClass = proxyChain.getTargetClass();

        begin(targetMethod, methodParams, targetClass);
        try {
            if(intercept(targetMethod, methodParams, targetClass)){
                before(targetMethod, methodParams, targetClass);
                result = proxyChain.doProxy();// 回调
                after(targetMethod, methodParams, targetClass, result);
            }else{
                result = proxyChain.doProxy();
            }
        } catch (Exception e) {
            logger.error("执行代理方法异常" + e);
            afterThrowing(targetMethod, methodParams, targetClass, e);
            throw e;
        } finally {
            afterReturing(targetMethod, methodParams, targetClass);
        }
        return result;
    }

    /**
     * 设置拦截过滤条件，针对哪些方法进行增强
     * @param targetMethod
     * @param methodParams
     * @param targetClass
     * @return
     * @throws Throwable
     */
    public boolean intercept(Method targetMethod, Object[] methodParams, Class<?> targetClass) throws Throwable{
        return true;
    }

    /**
     * 进入方法时执行
     * @param targetMethod
     * @param methodParams
     * @param targetClass
     * @throws Throwable
     */
    public void begin(Method targetMethod, Object[] methodParams, Class<?> targetClass) throws Throwable{}

    /**
     * 前置增强（方法执行前执行）
     * @param targetMethod
     * @param methodParams
     * @param targetClass
     * @throws Throwable
     */
    public void before(Method targetMethod, Object[] methodParams, Class<?> targetClass) throws Throwable{}

    /**
     * 后置增强（方法执行后执行）
     * @param targetMethod
     * @param methodParams
     * @param targetClass
     * @param result
     * @throws Throwable
     */
    public void after(Method targetMethod, Object[] methodParams, Class<?> targetClass, Object result) throws Throwable{}

    /**
     * 抛出增强（发生异常时执行）
     * @param targetMethod
     * @param methodParams
     * @param targetClass
     * @param e
     */
    public void afterThrowing(Method targetMethod, Object[] methodParams, Class<?> targetClass, Throwable e){}

    /**
     * 返回后增强（方法结束前执行）
     * @param targetMethod
     * @param methodParams
     * @param targetClass
     * @throws Throwable
     */
    public void afterReturing(Method targetMethod, Object[] methodParams, Class<?> targetClass) throws Throwable{}
}
