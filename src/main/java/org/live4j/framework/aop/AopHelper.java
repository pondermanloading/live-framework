package org.live4j.framework.aop;

import org.live4j.framework.core.InstanceFactory;
import org.live4j.framework.aop.annotation.Aspect;
import org.live4j.framework.aop.annotation.AspectOrder;
import org.live4j.framework.aop.proxy.Proxy;
import org.live4j.framework.aop.proxy.ProxyManager;
import org.live4j.framework.core.scanner.ClassHelper;
import org.live4j.framework.core.scanner.ClassScanner;
import org.live4j.framework.ioc.BeanHelper;
import org.live4j.framework.tx.TransactionProxy;
import org.live4j.framework.tx.annotation.Service;
import org.live4j.framework.util.ClassUtil;
import org.live4j.framework.util.ReflectionUtil;
import org.live4j.framework.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Title: AOP的助手类
 * Description: 封装映射关系K=目标类 V=切面类集合，在bean容器中注册目标类的动态代理
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/9 09:49
 */
public final class AopHelper {
    private static final Logger logger = LoggerFactory.getLogger(AopHelper.class);
    // 类的扫描器
    private static ClassScanner classScanner = InstanceFactory.getClassScanner();
    // 封装映射关系K=目标类 V=切面类集合，在bean容器中注册所有目标类的动态代理
    static{
        try {
            // 1.映射关系[1] K=切面类 V=目标类集合
            Map<Class<?>, Set<Class<?>>> proxyMap = createProxyMap();
            // 2.映射关系[2] K=目标类 V=切面类集合
            Map<Class<?>, List<Proxy>> targetMap = createTargetMap(proxyMap);
            for (Map.Entry<Class<?>, List<Proxy>> entry : targetMap.entrySet()) {
                Class<?> targetClass = entry.getKey();// 目标加载类
                List<Proxy> proxyList = entry.getValue();// 目标类的切面类集合
                Object proxy = ProxyManager.createProxy(targetClass, proxyList);// 目标类的动态代理
                // 在bean容器中注册目标类的动态代理
                BeanHelper.setBean(targetClass, proxy);
            }
        } catch(Exception ex){
            logger.error("AOP加载失败" + ex);
        }
    }

    /**
     * 映射关系[1] K=切面类 V=目标类集合
     * @return
     */
    private static Map<Class<?>, Set<Class<?>>> createProxyMap() throws Exception{
        Map<Class<?>, Set<Class<?>>> proxyMap = new LinkedHashMap<Class<?>, Set<Class<?>>>();
        addAspectProxy(proxyMap);       // [1.1] K=普通切面类 V=目标类集合
        addTransactionProxy(proxyMap);  // [1.2] K=事务切面类 V=目标类集合
        return proxyMap;
    }

    /**
     * K=普通切面类 V=目标类集合
     * @return
     */
    private static void addAspectProxy(Map<Class<?>, Set<Class<?>>> proxyMap){
        // 普通切面类集合
        Set<Class<?>> proxySet = ClassHelper.getClassSetBySuper(AspectProxy.class);
        // 排序普通切面类集合
        proxySet = sortAspectPorxy(proxySet);
        for(Class<?> proxyClass : proxySet){
            if(proxyClass.isAnnotationPresent(Aspect.class)){
                // 注解对象
                Aspect aspect = proxyClass.getAnnotation(Aspect.class);
                // 返回目标类集合
                Set<Class<?>> targetClassSet = createTargetClassSet(aspect);
                proxyMap.put(proxyClass, targetClassSet);
            }
        }
    }

    /**
     * K=事务切面类 V=目标类集合
     * @return
     */
    private static void addTransactionProxy(Map<Class<?>, Set<Class<?>>> proxyMap){
        Set<Class<?>> targetClassSet = ClassHelper.getClassSetByAnnotation(Service.class);
        proxyMap.put(TransactionProxy.class, targetClassSet);
    }

    /**
     * 排序普通切面类
     * @param proxySet 普通切面类集合
     */
    private static Set<Class<?>> sortAspectPorxy(Set<Class<?>> proxySet){
        TreeSet<Class<?>> sortProxySet = new TreeSet<Class<?>>(new Comparator<Class<?>>(){
            public int compare(Class<?> aspect1, Class<?> aspect2){
                // 判断是否有排序序号
                if(aspect1.isAnnotationPresent(AspectOrder.class) || aspect2.isAnnotationPresent(AspectOrder.class)){
                    return getAspectValue(aspect1) - getAspectValue(aspect2);
                }else{// 均没有排序序号，按自然排序
                    return aspect1.hashCode() - aspect2.hashCode();
                }
            }
            private int getAspectValue(Class<?> aspectClass){
                return aspectClass.getAnnotation(AspectOrder.class) != null ? aspectClass.getAnnotation(AspectOrder.class).value() : 0;
            }
        });
        sortProxySet.addAll(proxySet);
        return sortProxySet;
    }

    /**
     * @param aspect 注解类
     * @return 返回目标类集合
     */
    private static Set<Class<?>> createTargetClassSet(Aspect aspect){
        Set<Class<?>> targetClassSet = new HashSet<Class<?>>(); // 目标类集合
        String packgeName = aspect.pk();  // 指定包
        String cls = aspect.cls();  // 指定类
        Class<? extends Annotation> annotationClass = aspect.annotation();  // 指定注解
        // 判断指定包
        if(StringUtil.isNotEmpty(packgeName)){
            // 判断指定类
            if(StringUtil.isNotEmpty(cls)){
                // 返回指定类的加载类
                targetClassSet.add(ClassUtil.loadClass(packgeName + "." + cls));
            }else{
                // 判断注解
                if(null != annotationClass && !annotationClass.equals(Aspect.class)){
                    // 返回指定包下的标记指定注解的加载类
                    targetClassSet.addAll(classScanner.getClassSetByAnnotation(packgeName, annotationClass));
                }else{ // 返回指定包下的加载类
                    targetClassSet.addAll(classScanner.getClassSet(packgeName));
                }
            }
        }else{// 包为空，返回标记指定注解的加载类
            if(null != annotationClass && !annotationClass.equals(Aspect.class)) {
                targetClassSet = ClassHelper.getClassSetByAnnotation(annotationClass);
            }
        }
        return targetClassSet;
    }

    /**
     * 映射关系[2] K=目标类 V=切面类集合
     * @return
     */
    private static Map<Class<?>, List<Proxy>> createTargetMap(Map<Class<?>, Set<Class<?>>> proxyMap) throws Exception{
        Map<Class<?>, List<Proxy>> targetMap = new HashMap<Class<?>, List<Proxy>>();
        for(Map.Entry<Class<?>, Set<Class<?>>> entry : proxyMap.entrySet()){
            // 切面类
            Class<?> proxyClass = entry.getKey();
            // 目标类集合
            Set<Class<?>> targetClassSet = entry.getValue();
            for(Class<?> targetClass : targetClassSet){
                Proxy proxy = (Proxy) ReflectionUtil.newInstanceByClass(proxyClass);
                if(targetMap.containsKey(targetClass)){
                    targetMap.get(targetClass).add(proxy);
                } else{
                    // 切面类集合
                    List<Proxy> proxyList = new ArrayList<Proxy>();
                    proxyList.add(proxy);
                    targetMap.put(targetClass, proxyList);
                }
            }
        }
        return targetMap;
    }
}
