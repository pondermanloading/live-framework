package org.live4j.framework.aop.proxy;

/**
 * Title: 切面类接口
 * Description: 执行增强方法
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/8 08:21
 */
public interface Proxy {
    /**
     * 执行增强方法
     * @param proxyChain 代理链
     * @return 方法返回值
     */
    Object doProxy(ProxyChain proxyChain) throws Throwable;
}
