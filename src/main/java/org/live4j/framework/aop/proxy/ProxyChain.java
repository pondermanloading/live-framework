package org.live4j.framework.aop.proxy;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Title: 代理链
 * Description: 封装代理链接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/8 08:23
 */
public class ProxyChain {
    // 目标对象
    private final Object targetObject;
    // 目标对象的方法对象
    private final Method targetMethod;
    // 目标对象的方法参数
    private final Object[] methodParams;
    // 目标对象的方法对象代理
    private final MethodProxy methodProxy;
    // 目标加载类
    private final Class<?> targetClass;

    // 代理链列表（目标类的所有切面类）
    private List<Proxy> proxyList;
    // 代理个数计数器
    private int proxyIndex = 0;

    public ProxyChain(Object targetObject, Method targetMethod, Object[] methodParams, MethodProxy methodProxy, Class<?> targetClass, List<Proxy> proxyList) {
        this.targetObject = targetObject;
        this.targetMethod = targetMethod;
        this.methodParams = methodParams;
        this.methodProxy = methodProxy;
        this.targetClass = targetClass;
        this.proxyList = proxyList;
    }

    /**
     * 返回目标对象的方法对象
     * @return
     */
    public Method getTargetMethod(){
        return targetMethod;
    }

    /**
     * 返回目标对象的方法参数
     * @return
     */
    public Object[] getMethodParams(){
        return methodParams;
    }

    /**
     * 返回目标加载类
     * @return
     */
    public Class<?> getTargetClass(){
        return targetClass;
    }

    /**
     * 执行切面类的增强方法
     * @return 方法返回值
     * @throws Throwable
     */
    public Object doProxy() throws Throwable {
        Object methodResult;
        if(proxyIndex < proxyList.size()){
            // 执行增强方法
            methodResult = proxyList.get(proxyIndex ++).doProxy(this);
        }else{
            // 执行目标方法
            methodResult = methodProxy.invokeSuper(targetObject, methodParams);
        }
        return methodResult;
    }

}
