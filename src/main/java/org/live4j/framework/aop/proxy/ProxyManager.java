package org.live4j.framework.aop.proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Title: 代理管理类
 * Description: 创建目标类的动态代理
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/8 09:46
 */
public class ProxyManager {

    /**
     * @param targetClass 目标类的加载类
     * @param proxyList 目标类的切面类集合
     * @param <T>
     * @return 返回目标类的动态代理
     */
    public static <T> T createProxy(final Class<T> targetClass, final List<Proxy> proxyList){
        return (T) Enhancer.create(targetClass, new MethodInterceptor(){
            public Object intercept(Object targetObject, Method targetMethod,
                                    Object[] methodParams, MethodProxy methodProxy) throws Throwable {
                return new ProxyChain(targetObject, targetMethod,
                        methodParams, methodProxy, targetClass,
                        proxyList).doProxy();
            }
        });
    }
}
