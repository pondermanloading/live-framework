package org.live4j.framework.dao.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Title:分页
 * Description:
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/8 19:02
 */
public class Pager<T> implements Serializable {
    private Integer pageNumber;// 当前页
    private Integer pageSize;// 每页大小
    private Integer totalRecord;// 总记录数
    private Integer totalPage;// 总页数
    private List<T> recordList;// 数据列表

    public Pager(){}

    public Pager(Integer pageNumber, Integer pageSize, Integer totalRecord, Integer totalPage, List<T> recordList) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalRecord = totalRecord;
        this.totalPage = totalPage;
        this.recordList = recordList;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public List<T> getRecordList() {
        return recordList;
    }

    public boolean isFirstPage(){
        return 1 == pageNumber;
    }

    public boolean isLastPage(){
        return totalPage == pageNumber;
    }

    public boolean isPrevPage(){
        return pageNumber > 1 && pageNumber <= totalPage;
    }

    public boolean isNextPage(){
        return pageNumber > 1;
    }
}
