package org.live4j.framework.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Title:数据访问器
 * Description:
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/9/8 20:25
 */
public interface DataAccessor {

    /**
     * 查询对应的[实体]，返回[单条记录]
     * 解释：查询多个字段的一条数据，映射成一个POJO实体
     * @param entityClass
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    <T> T queryEntity(Class<T> entityClass, String sql, Object... params);

    /**
     * 查询对应的[实体列表]，返回[多条记录]
     * 解释：查询多个字段的多条数据，映射成多个POJO实体
     * @param entityClass
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    <T> List<T> queryEntityList(Class<T> entityClass, String sql, Object... params);

    /**
     * 查询对应的[实体]，返回[单条记录]（成员变量名称：数据）
     * 解释：查询多个字段的一条数据，映射成一个Map<K, V>，K=成员变量名称，V=成员变量值
     * @param entityClass
     * @param sql
     * @param params
     * @param <K>
     * @param <V>
     * @return
     */
    <K, V> Map<K, V> queryEntityMap(Class<V> entityClass, String sql, Object... params);

    /**
     * 查询对应的[数据]，返回[单条记录]
     * 解释：查询多个字段的一条数据，仅保留数据，不保留字段名称，如["张三"，23]
     * @param sql
     * @param params
     * @return
     */
    Object[] queryArray(String sql, Object... params);

    /**
     * 查询对应的[数据列表]，返回[多条记录]
     * 解释：查询多个字段的多条数据，仅保留数据，不保留字段名称，如[["张三"，23]，["李四"，22]]
     * @param sql
     * @param params
     * @return
     */
    List<Object[]> queryArrayList(String sql, Object... params);

    /**
     * 查询对应的[数据]，返回[单条记录]（数据库字段：数据）
     * 解释：查询多个字段的一条数据，保留字段名称和数据，如｛name："张三"，age：23｝
     * @param sql
     * @param params
     * @return
     */
    Map<String, Object> queryMap(String sql, Object... params);

    /**
     * 查询对应的[数据]，返回[多条记录]（数据库字段：数据）
     * 解释：查询多个字段的多条数据，保留字段名称和数据，如[{name："张三"，age：23}，{name："李四"，age：22}]
     * @param sql
     * @param params
     * @return
     */
    List<Map<String, Object>> queryMapList(String sql, Object... params);

    /**
     * 查询对应的[数据]，返回[单个数据]
     * 解释：查询一个字段的一个数据，如name字段的值，张三
     * @param sql
     * @param params
     * @return
     */
    <T> T queryColumn(String sql, Object... params);

    /**
     * 查询对应的[数据]，返回[多个数据]
     * 解释：查询一个字段的多个数据，如name字段的值，张三、李四
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    <T> List<T> queryColumnList(String sql, Object... params);

    /**
     * 查询指定[字段名]对应的[数据]，返回[多个数据]
     * 解释：查询一个指定字段的多个数据，如name字段的值，{name：{name："张三"，name："李四"}}
     * @param column
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    <T> Map<T, Map<String, Object>> queryColumnMap(String column, String sql, Object... params);

    /**
     * 查询记录条数，返回总记录数
     * @param sql
     * @param params
     * @return
     */
    long queryCount(String sql, Object... params);

    /**
     * 执行更新操作，包括（insert，delete，update），返回影响的记录数
     * @param sql
     * @param params
     * @return
     */
    int update(String sql, Object... params);

    /**
     * 插入一条记录，返回插入后的主键
     * @param sql
     * @param params
     * @return
     */
    Serializable insertReturnPK(String sql, Object... params);
}
