package org.live4j.framework.dao;

import org.live4j.framework.core.InstanceFactory;
import org.live4j.framework.ds.DataSourceDirector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 * Title: 数据库连接帮助类
 * Description: 封装数据库连接相关接口
 * Copyright: 2017
 * Author: Oak
 * Create Time:2017/10/19 16:15
 */
public class DataBaseHelper {
    private static final Logger logger = LoggerFactory.getLogger(DataBaseHelper.class);

    // 数据库连接（线程局部变量，每个线程拥有自己的Connection，线程之间隔离）
    private static final ThreadLocal<Connection> connContainer = new ThreadLocal<Connection>();

    // 数据源指导者
    private static final DataSourceDirector dataSourceDirector = InstanceFactory.getDataSourceDirector();

    // 数据访问器
    private static final DataAccessor dataAccessor = InstanceFactory.getDataAccessor();

    /**
     * 返回数据源
     * @return
     */
    public static DataSource getDataSource(){
        return dataSourceDirector.getDataSource();
    }

    /**
     * 返回当前线程的数据库连接
     * @return
     */
    public static Connection getConnection(){
        // 返回线程局部变量的Connection数据库连接
        Connection conn = connContainer.get();
        if(null == conn){
            try{
                conn = getDataSource().getConnection();
                if(null != conn){
                    // 存在线程局部变量
                    connContainer.set(conn);
                }
            }catch(Exception ex){
                logger.error("获取数据库连接失败" + ex);
                throw new RuntimeException("获取数据库连接失败" + ex);
            }
        }
        return conn;
    }

    /**
     * 开启事务
     */
    public static void beginTransaction(){
        Connection conn = getConnection();
        if(null != conn){
            try{
                conn.setAutoCommit(false);
            }catch (Exception ex){
                logger.error("开启事务失败" + ex);
                throw new RuntimeException("开启事务失败" + ex);
            }finally{
                connContainer.set(conn);
            }
        }
    }

    /**
     * 提交事务
     */
    public static void commitTransaction(){
        Connection conn = getConnection();
        if(null != conn) {
            try {
                conn.commit();
            } catch (Exception ex) {
                logger.error("提交事务失败" + ex);
                throw new RuntimeException("提交事务失败" + ex);
            } finally {
                if (null != conn) {
                    try {
                        conn.close();
                    } catch (Exception ex) {
                        logger.error("关闭数据库连接失败" + ex);
                    }
                }
                connContainer.remove();
            }
        }
    }

    /**
     * 回滚事务
     */
    public static void rollbackTransaction(){
        Connection conn = getConnection();
        if(null != conn) {
            try {
                conn.rollback();
            } catch (Exception ex) {
                logger.error("回滚事务失败" + ex);
                throw new RuntimeException("回滚事务失败" + ex);
            } finally {
                if (null != conn) {
                    try {
                        conn.close();
                    } catch (Exception ex) {
                        logger.error("关闭数据库连接失败" + ex);
                    }
                }
                connContainer.remove();
            }
        }
    }

    /**
     * 根据 SQL 语句查询 Entity
     */
    public static <T> T queryEntity(Class<T> entityClass, String sql, Object... params) {
        return dataAccessor.queryEntity(entityClass, sql, params);
    }

    /**
     * 根据 SQL 语句查询 Entity 列表
     */
    public static <T> List<T> queryEntityList(Class<T> entityClass, String sql, Object... params) {
        return dataAccessor.queryEntityList(entityClass, sql, params);
    }

    /**
     * 根据 SQL 语句查询 Entity 映射（Field Name => Field Value）
     */
    public static <K, V> Map<K, V> queryEntityMap(Class<V> entityClass, String sql, Object... params) {
        return dataAccessor.queryEntityMap(entityClass, sql, params);
    }

    /**
     * 根据 SQL 语句查询 Array 格式的字段（单条记录）
     */
    public static Object[] queryArray(String sql, Object... params) {
        return dataAccessor.queryArray(sql, params);
    }

    /**
     * 根据 SQL 语句查询 Array 格式的字段列表（多条记录）
     */
    public static List<Object[]> queryArrayList(String sql, Object... params) {
        return dataAccessor.queryArrayList(sql, params);
    }

    /**
     * 根据 SQL 语句查询 Map 格式的字段（单条记录）
     */
    public static Map<String, Object> queryMap(String sql, Object... params) {
        return dataAccessor.queryMap(sql, params);
    }

    /**
     * 根据 SQL 语句查询 Map 格式的字段列表（多条记录）
     */
    public static List<Map<String, Object>> queryMapList(String sql, Object... params) {
        return dataAccessor.queryMapList(sql, params);
    }

    /**
     * 根据 SQL 语句查询指定字段（单条记录）
     */
    public static <T> T queryColumn(String sql, Object... params) {
        return dataAccessor.queryColumn(sql, params);
    }

    /**
     * 根据 SQL 语句查询指定字段列表（多条记录）
     */
    public static <T> List<T> queryColumnList(String sql, Object... params) {
        return dataAccessor.queryColumnList(sql, params);
    }

    /**
     * 根据 SQL 语句查询指定字段映射（多条记录）
     */
    public static <T> Map<T, Map<String, Object>> queryColumnMap(String column, String sql, Object... params) {
        return dataAccessor.queryColumnMap(column, sql, params);
    }

    /**
     * 根据 SQL 语句查询记录条数
     */
    public static long queryCount(String sql, Object... params) {
        return dataAccessor.queryCount(sql, params);
    }

    /**
     * 执行更新语句（包括：update、insert、delete）
     */
    public static int update(String sql, Object... params) {
        return dataAccessor.update(sql, params);
    }

    /**
     * 执行插入语句，返回插入后的主键
     */
    public static Serializable insertReturnPK(String sql, Object... params) {
        return dataAccessor.insertReturnPK(sql, params);
    }

}
